package networking;

import tester.MapPanel;

public abstract class GameBoardThreading extends MapPanel implements Runnable {
    private final String name;
    private volatile Thread thread;
    private volatile boolean isRunning;

    public GameBoardThreading(String name) {
        isRunning = false;
        this.name = name;
    }

    public synchronized void start() {
        if (!isRunning) {
            isRunning = true;
            thread = new Thread(this);
            thread.start();
        }
    }

    public synchronized void stop() {
        if (isRunning) {
            thread.interrupt();
            while (isRunning) {
                try {
                    wait();
                } catch (InterruptedException ex) {
                }
            }
        }
    }

    @Override
    public void run() {
        logic();
        synchronized (GameBoardThreading.this) {
            isRunning = false;
            notifyAll();
        }
        System.out.println(name + ": stopped");
    }

    protected abstract void logic();
}
