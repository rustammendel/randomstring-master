package networking;

import java.io.IOException;
import java.io.InputStream;

public class GameStreamReader {
    public final byte[] buffer;
    private final InputStream input;
    private final int refreshInterval;
    private final int maxIdleTime;
    private final int frameSize;
    private int offset = 0, idleTime = 0;
    private boolean isClosed = false;

    /**
     * @param is              The stream to read from
     * @param refreshInterval refresh interval in msec
     * @param maxIdleTime     maximum idle time in msec when stream is considered as broken
     * @param frameSize       reader reads frames from the stream of the same size in sequence
     */
    public GameStreamReader(InputStream is, int refreshInterval, int maxIdleTime, int frameSize) {
        this.input = is;
        this.refreshInterval = refreshInterval;
        this.maxIdleTime = maxIdleTime;
        this.frameSize = frameSize;
        this.buffer = new byte[frameSize];
    }

    public boolean readFrame() {
        if (isClosed) return false;
        try {
            if (input.available() > 0) {
                int len = input.read(buffer, offset, frameSize - offset);
                offset += len;
                idleTime = 0;
                if (offset == len) {
                    offset = 0;
                    return true;
                }
            } else {
                idleTime += refreshInterval;
                if (maxIdleTime <= idleTime) {
                    isClosed = true;
                }
            }
        } catch (IOException ex) {
            isClosed = true;
        }
        return false;
    }

    public boolean isClosed() {
        return isClosed;
    }
}
