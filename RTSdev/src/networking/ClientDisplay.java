package networking;

public interface ClientDisplay {
    void refresh();

    void connectionLost();

    void cannotConnect();
}
