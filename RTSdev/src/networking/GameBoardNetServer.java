package networking;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;


public class GameBoardNetServer extends GameBoardThreading implements ServerDisplay {
    private final int port;
    private final int refreshRate;
    private final Socket[] clients;
    private final InputStream[] inputs;
    private final OutputStream[] outputs;
    private ServerDisplay display;

    public GameBoardNetServer(int refreshRate, int port) {
        super("Server");
        this.port = port;
        this.refreshRate = refreshRate;
        int numofPlayers = players.size(); //Number of Players
        clients = new Socket[numofPlayers];
        inputs = new InputStream[numofPlayers];
        outputs = new OutputStream[numofPlayers];
        display = this; // Print on console
    }

    public void setDisplay(ServerDisplay display) {
        this.display = (display == null) ? this : display; // Messages on: Console / GUI
    }

    @Override
    protected void logic() {
        System.out.println("Server: started");

        if (!waitForClients()) {
            closeConnections();
            System.out.println("Server: an error occured during the connection phase");
            return;
        }
        display.allClientsAreConnected();

        int droppedClient = -1;

        while (!Thread.interrupted()) {
            for (int i = 0; i < inputs.length; i++) {
                InputStream is = inputs[i];
                try {
                    if (is != null && is.available() > 0) {
                        //TODO: change this lines to neccessary i/o data;

                       /*
                        Direction d = Direction.values()[is.read()];
                        changePlayerDirection(i, d);
                        System.out.println("Server: Client-" + i + " turns to " + d);
                        */


                    }
                } catch (IOException ex) {
                    inputs[i] = null;
                    outputs[i] = null;
                    droppedClient = i;
                    break;
                }
            }
            //TODO: change this lines to method calls with updated data:
            /*
            movePlayers();

             */
            byte[] boardBytes = toByteArray();

            for (int i = 0; i < outputs.length; i++) {
                try {
                    if (outputs[i] != null) outputs[i].write(boardBytes);
                } catch (IOException ex) {
                    inputs[i] = null;
                    outputs[i] = null;
                    droppedClient = i;
                    break;
                }
            }
            if (droppedClient != -1) break;
            try {
                Thread.sleep(refreshRate);
            } catch (InterruptedException ex) {
                System.out.println("Server: interrupted");
                break;
            }
        }
        closeConnections();
        if (droppedClient != -1) {
            display.connectionLost(droppedClient);
        }
    }

    private boolean waitForClients() {
        ServerSocket serverSocket;
        int numClients = 0;
        //TODO: something is wrong here :(
        // byte[] boardBytes = toByteArray();

        try {
            serverSocket = new ServerSocket(port);
            serverSocket.setSoTimeout(refreshRate); // in milliseconds
            while (!Thread.interrupted()) {
                try {
                    Socket serviceSocket = serverSocket.accept();
                    clients[numClients] = serviceSocket;
                    inputs[numClients] = serviceSocket.getInputStream();
                    outputs[numClients] = serviceSocket.getOutputStream();
                    System.out.println("Server: Client-" + numClients + " is connected...");
                    if (++numClients == players.size()) break;
                } catch (SocketTimeoutException toe) {
                }
                Thread.sleep(refreshRate);
                for (int i = 0; i < numClients; i++) {
                    //todo fixthis
                    //  outputs[i].write(boardBytes); // Send dummy info, to keep the connection alive
                }
            }
            serverSocket.close();
            return (numClients == players.size());
        } catch (IOException io) {
            return false;
        } catch (InterruptedException ie) {
            return false;
        }
    }

    private void closeConnections() {
        for (int i = 0; i < players.size(); i++) {
            if (inputs[i] != null) try {
                inputs[i].close();
            } catch (IOException ex) {
            }
            if (outputs[i] != null) try {
                outputs[i].close();
            } catch (IOException ex) {
            }
            if (clients[i] != null) try {
                clients[i].close();
            } catch (IOException ex) {
            }
        }
    }

    @Override
    public void connectionLost(int client) {
        System.out.println("Server: Connection to Client-" + client + " is lost");
    }

    @Override
    public void cannotOpenServer() {
        System.out.println("Server: cannot open port " + port);
    }

    @Override
    public void allClientsAreConnected() {
        System.out.println("Server: all clients are connected");
    }
}
