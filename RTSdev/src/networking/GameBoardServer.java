package networking;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class GameBoardServer extends GameBoardThreading implements ServerDisplay {
    private final InputStream[] inputs;
    private final OutputStream[] outputs;
    private final int refreshRate;
    private ServerDisplay display;

    /**
     * Creates a new GameBoard with the given scale, and
     * each client is represented with a Data Input/Output Stream pair
     *
     * @param is Client end points to read from
     * @param os Client end points to write to
     */
    public GameBoardServer(int refreshRate, InputStream[] is, OutputStream[] os) {
        super("Server");
        this.refreshRate = refreshRate;
        inputs = is;
        outputs = os;
        display = this;
    }

    public void setDisplay(ServerDisplay display) {
        this.display = (display == null) ? this : display; // Messages on: Console / GUI
    }

    @Override
    protected void logic() {
        System.out.println("Server: started");
        int droppedClient = -1;

        while (!Thread.interrupted()) {
            for (int i = 0; i < inputs.length; i++) {
                InputStream is = inputs[i];
                try {
                    if (is != null && is.available() > 0) {
                        // Direction d = Direction.values()[is.read()];
                        // changePlayerDirection(i, d);

                        //  // System.out.println("Server: Client-" + i + " turns to " + d);
                    }
                } catch (IOException ex) {
                    inputs[i] = null;
                    outputs[i] = null;
                    droppedClient = i;
                    break;
                }
            }
            // movePlayers();
            byte[] boardBytes = toByteArray();

            for (int i = 0; i < outputs.length; i++) {
                try {
                    if (outputs[i] != null) outputs[i].write(boardBytes);
                } catch (IOException ex) {
                    inputs[i] = null;
                    outputs[i] = null;
                    droppedClient = i;
                    break;
                }
            }
            if (droppedClient != -1) break;
            try {
                Thread.sleep(refreshRate);
            } catch (InterruptedException ex) {
                System.out.println("Server: interrupted");
                break;
            }
        }
        if (droppedClient != -1) {
            display.connectionLost(droppedClient);
        }
    }

    @Override
    public void connectionLost(int client) {
        System.out.println("Server: Connection to Client-" + client + " is lost");
    }

    @Override
    public void cannotOpenServer() {
    }

    @Override
    public void allClientsAreConnected() {
    }
}
