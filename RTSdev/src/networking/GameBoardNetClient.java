package networking;

import tester.GameMode;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
//import model.Direction;

public class GameBoardNetClient extends GameBoardThreading implements ClientDisplay {
    private String address;
    private int port;
    private Socket server;
    private InputStream input;
    private OutputStream output;
    private ClientDisplay display;
    private GameMode gameMode;

    public GameBoardNetClient() {
        super("Client");
        this.address = "127.0.0.1";
        this.port = 5000;
        this.display = this; // Print on console
    }

    public void setServerAddress(String address, int port) {
        this.address = address;
        this.port = port;
    }

    public void setDisplay(ClientDisplay display) {
        this.display = (display == null) ? this : display; // Messages on: Console / GUI
    }

    //TODO:change methods below:
/*
    public void changePlayerDirection(Direction d) {
        try {
            if (output != null) output.write(d.ordinal());
        } catch (IOException ex) {
            stop();
            closeConnection(true);
        }
    }


    @Override
    public void movePlayers() {
    }


 */
    @Override
    protected void logic() {
        if (!waitForServer()) return;

        boolean createReport = false;
        GameStreamReader reader = new GameStreamReader(input, 10, 1000, representationSize);
        System.out.println("Client: started");
        try {
            while (!Thread.interrupted()) {
                if (reader.readFrame()) {
                    synchronized (GameBoardNetClient.this) {
                        fromByteArray(reader.buffer);
                    }
                    display.refresh();
                } else if (reader.isClosed()) {
                    createReport = true;
                    break;
                } else {
                    Thread.sleep(10);
                }
            }
        } catch (InterruptedException iex) {
            System.out.println("Client: interrupted");
        }

        closeConnection(createReport);
    }

    private boolean waitForServer() {
        try {
            server = new Socket(address, port);
            input = server.getInputStream();
            output = server.getOutputStream();
            return true;
        } catch (IOException ex) {
            display.cannotConnect();
            closeConnection(false);
        }
        return false;
    }

    private synchronized void closeConnection(boolean report) {
        if (input != null) try {
            input.close();
        } catch (IOException ex) {
        }
        if (output != null) try {
            output.close();
        } catch (IOException ex) {
        }
        if (server != null) try {
            server.close();
        } catch (IOException ex) {
        }
        input = null;
        output = null;
        server = null;
        if (report) display.connectionLost();
    }

    @Override
    public void refresh() {
    }

    @Override
    public void connectionLost() {
        System.out.println("Client: connection is lost");
    }

    @Override
    public void cannotConnect() {
        System.out.println("Client: cannot connect to server");
    }
}
