package networking;

public interface ServerDisplay {
    void connectionLost(int client);

    void cannotOpenServer();

    void allClientsAreConnected();
}
