package networking;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class GameBoardClient extends GameBoardThreading implements ClientDisplay {
    private final ClientDisplay display;
    private InputStream is;
    private OutputStream os;

    public GameBoardClient(InputStream is, OutputStream os, ClientDisplay display) {
        super("Client");
        this.is = is;
        this.os = os;
        // Print on console, if there is no real GUI (for testing purposes)
        this.display = (display == null) ? this : display;
    }

    //TODO: fix methods below :

/*
    public void changePlayerDirection(Direction d) {
        try {
            if (os != null) os.write(d.ordinal());
        } catch (IOException ex) {
            closeStreams(true);
        }
    }

    @Override
    public void movePlayers() {
    }
*/

    @Override
    protected void logic() {
        boolean createReport = false;
        GameStreamReader reader = new GameStreamReader(is, 10, 1000, representationSize);
        System.out.println("Client: started");
        try {
            while (!Thread.interrupted()) {
                if (reader.readFrame()) {
                    synchronized (GameBoardClient.this) {
                        fromByteArray(reader.buffer);
                    }
                    display.refresh();
                } else if (reader.isClosed()) {
                    createReport = true;
                    break;
                } else {
                    Thread.sleep(10);
                }
            }
        } catch (InterruptedException iex) {
            System.out.println("Client: interrupted");
        }
        closeStreams(createReport);
    }

    /**
     * Close the input/output streams
     *
     * @param report If true, it calls at the end the connectionLost()
     *               method of the display.
     */
    private synchronized void closeStreams(boolean report) {
        try {
            if (is != null) is.close();
        } catch (IOException e) {
        }
        try {
            if (os != null) os.close();
        } catch (IOException e) {
        }
        is = null;
        os = null;
        if (report) display.connectionLost();
    }

    @Override
    public void refresh() {
    }

    @Override
    public void connectionLost() {
        System.out.println("Client: connection is lost");
    }

    @Override
    public void cannotConnect() {
    }
}
