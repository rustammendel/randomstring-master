package tester;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Building extends Item implements ActionListener {

    //Visual properties
    private static final String imagePath = "resources/buildings/";
    //Properties:
    private static int count = 0;
    private static final String healthBarImagePath = "src/resources/character/healthBar/";
    public Point coordOfHealthButton;
    private final int[] button_dim_x = {0, 80, 80, 80, 40};
    private final int[] button_dim_y = {0, 80, 80, 80, 120};
    private final int block_size = 40;
    private final ImageIcon image;
    private int buildingType;
    private final Point coordOfBuild;
    private int headquarterLife;
    private HealthBar healthBar;


    public Building(int price, int buildingType, Point p) {
        super(count, p, price);
        this.buildingType = buildingType;
        this.button = new JButton();

        count = count + 1;
        coordOfBuild = new Point(0, 0);
        coordOfBuild.x = p.x * block_size;
        coordOfBuild.y = p.y * block_size;

        coordOfHealthButton = new Point();
        coordOfHealthButton.x = coordOfBuild.x;
        coordOfHealthButton.y = (coordOfBuild.y - 1);

        healthBar = new HealthBar();
        button.setOpaque(false);
        button.setContentAreaFilled(false);
        button.setBorderPainted(false);
        button.setLayout(null);
        button.setBounds(coordOfBuild.x, coordOfBuild.y, button_dim_x[buildingType], button_dim_y[buildingType]);
        button.addActionListener(this::actionPerformed);
        image = new ImageIcon(imagePath + "b" + buildingType + ".png");


        try {
            button.setIcon(image);
        } catch (Exception ex) {
            System.out.println(ex);
        }

        if (buildingType == 4) {
            GameEngine.addObstacle(p);
            GameEngine.addObstacle(new Point(p.x, p.y + 1));
            GameEngine.addObstacle(new Point(p.x, p.y + 2));
        } else {
            GameEngine.addObstacle(p);
            GameEngine.addObstacle(new Point(p.x, p.y + 1));
            GameEngine.addObstacle(new Point(p.x + 1, p.y));
            GameEngine.addObstacle(new Point(p.x + 1, p.y + 1));
        }

        healthBar.button.setBounds(coordOfHealthButton.x, coordOfHealthButton.y, 40, 7);

    }

    public static String getImagePath() {
        return imagePath;
    }

    public static int getCount() {
        return count;
    }

    public void actionPerformed(ActionEvent e) {
        MapPanel.selectedBuilding = this;
        System.out.println("You selected building");
        if (MapPanel.selectedActionType == 2) {
            MapPanel.selectedBuilding.underAttack(Character.characters.get(0));
        }
    }

    public JButton getButton() {
        return button;
    }

    public int getBuildingType() {
        return buildingType;
    }

    public void setBuildingType(int buildingType) {
        this.buildingType = buildingType;
    }

    public int getHeadquarterLife() {
        return headquarterLife;
    }

    public void setHeadquarterLife(int headquarterLife) {
        this.headquarterLife = headquarterLife;
    }

    public void underAttack(Character character) {
        int difference = (int) (getP().getX() - character.getP().getX());
        System.out.println(difference);
        if (MapPanel.selectedActionType == 2) {
            if (difference == -1) {
                headquarterLife -= character.getDamageLevel();
                System.out.println("Brat personaj budu:" + character.getP());
                System.out.println("Qaqa headquarterimin lifeni saldim asagi:" + headquarterLife);
                setHealthBarImage();
            }
        }
        if (headquarterLife <= 0) {
            JFrame frame = new JFrame();
            JOptionPane optionPane = new JOptionPane("Game Over, Player 1 won", JOptionPane.PLAIN_MESSAGE);
            JButton exitButton = new JButton("EXIT");
            exitButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    System.exit(1);
                }
            });
            JDialog dialog = optionPane.createDialog(frame, "Manual Creation");
            exitButton.setVisible(true);
            optionPane.add(exitButton);
            dialog.setVisible(true);
        }
    }

    public void setHealthBarImage() {
        if (headquarterLife == 75) {
            ImageIcon imageIcon = new ImageIcon(healthBarImagePath + 75 + ".png");
            healthBar.getButton().setIcon(imageIcon);
        } else if (headquarterLife == 50) {
            ImageIcon imageIcon = new ImageIcon(healthBarImagePath + 50 + ".png");
            healthBar.getButton().setIcon(imageIcon);
        } else if (headquarterLife == 25) {
            ImageIcon imageIcon = new ImageIcon(healthBarImagePath + 25 + ".png");
            healthBar.getButton().setIcon(imageIcon);
        } else if (headquarterLife == 0) {
            ImageIcon imageIcon = new ImageIcon(healthBarImagePath + 0 + ".png");
            healthBar.getButton().setIcon(imageIcon);
        }

        System.out.println("Qaqa gorursen rengi basdim ee");
    }

    public HealthBar getHealthBar() {
        return healthBar;
    }

    public void setHealthBar(HealthBar healthBar) {
        this.healthBar = healthBar;
    }
}
