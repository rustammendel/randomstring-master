package tester;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GameWindow extends JPanel {

    public static String imagePath = "src/resources/window";
    public static JLabel availableCoins = new JLabel();
    public static JLabel availableSoldiers = new JLabel();
    public static JLabel availableBuildings = new JLabel();
    private static int turn = 0;
    public MapPanel mapPanel;
    public Image background;


    public GameWindow() {
        setLayout(new BorderLayout());

        mapPanel = new MapPanel();
        background = new ImageIcon(imagePath + "/paper.png").getImage();


        JPanel top_p = new JPanel() {
            @Override
            protected void paintComponent(Graphics grphcs) {
                super.paintComponent(grphcs);
                grphcs.drawImage(background, 0, 0, this.getWidth(), this.getHeight(), null);
            }
        };

        top_p.setLayout(new GridLayout(1, 3, 10, 0));
        top_p.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        JButton menu_b = new JButton("MAIN MENU", new ImageIcon(imagePath + "/menu_b.png"));
        menu_b.setBorder(BorderFactory.createEmptyBorder());
        menu_b.setHorizontalTextPosition(JButton.CENTER);
        menu_b.setContentAreaFilled(false);
        menu_b.setForeground(Color.lightGray);
        menu_b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Window.getCL().show(Window.getWindow(), "mainWindow");
            }
        });
        JLabel health_l = new JLabel("Health: ");
        health_l.setIcon(new ImageIcon(imagePath + "/hbar.png"));
        JLabel coins_l = new JLabel("Coins: ");

        coins_l.setIcon(new ImageIcon(imagePath + "/coin.png"));
        JLabel soldiers_l = new JLabel("Soldiers: ");
        soldiers_l.setIcon(new ImageIcon(imagePath + "/axe.png"));
        JLabel buildings_l = new JLabel("Buildings: ");
        buildings_l.setIcon(new ImageIcon(imagePath + "/tools.png"));

        top_p.add(health_l);
        top_p.add(coins_l);
        top_p.add(availableCoins);
        top_p.add(soldiers_l);
        top_p.add(availableSoldiers);
        top_p.add(buildings_l);
        top_p.add(availableBuildings);
        top_p.add(menu_b);


        JPanel side_p = new JPanel() {
            @Override
            protected void paintComponent(Graphics grphcs) {
                super.paintComponent(grphcs);
                grphcs.drawImage(background, 0, 0, this.getWidth(), this.getHeight(), null);
            }
        };
        side_p.setBorder(BorderFactory.createLineBorder(Color.BLACK));

        JPanel add_p = new JPanel() {
            @Override
            protected void paintComponent(Graphics grphcs) {
                super.paintComponent(grphcs);
                grphcs.drawImage(background, 0, 0, this.getWidth(), this.getHeight(), null);
            }
        };
        add_p.setLayout(new GridLayout(8, 1, 20, 20));

        JButton b1 = new JButton(new ImageIcon(Building.getImagePath() + "ab1" + ".png"));
        b1.setContentAreaFilled(false);
        b1.setBorder(BorderFactory.createEmptyBorder());
        b1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("building button clicked");
                MapPanel.mode = 2;
                MapPanel.selected_build_type = 1;
            }
        });
        JButton b2 = new JButton(new ImageIcon(Building.getImagePath() + "ab2" + ".png"));
        b2.setContentAreaFilled(false);
        b2.setBorder(BorderFactory.createEmptyBorder());
        b2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("building button clicked");
                MapPanel.mode = 2;
                MapPanel.selected_build_type = 2;
            }
        });
        JButton b3 = new JButton(new ImageIcon(Building.getImagePath() + "ab3" + ".png"));
        b3.setContentAreaFilled(false);
        b3.setBorder(BorderFactory.createEmptyBorder());
        b3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("building button clicked");
                MapPanel.mode = 2;
                MapPanel.selected_build_type = 3;
            }
        });
        JButton b4 = new JButton(new ImageIcon(Building.getImagePath() + "ab4" + ".png"));
        b4.setContentAreaFilled(false);
        b4.setBorder(BorderFactory.createEmptyBorder());
        b4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("building button clicked");
                MapPanel.mode = 2;
                MapPanel.selected_build_type = 4;
            }
        });

        JButton c1 = new JButton(new ImageIcon(Character.getImagePath() + "c1" + ".png"));
        c1.setContentAreaFilled(false);
        c1.setBorder(BorderFactory.createEmptyBorder());
        c1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Character button clicked");
                MapPanel.mode = 1;
            }
        });

        JButton c2 = new JButton(new ImageIcon(Character.getImagePath() + "c1" + ".png"));
        c2.setContentAreaFilled(false);
        c2.setBorder(BorderFactory.createEmptyBorder());
        c2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Character button clicked");
                MapPanel.mode = 11;
            }
        });

        JButton c3 = new JButton(new ImageIcon(Character.getImagePath() + "c1" + ".png"));
        c3.setContentAreaFilled(false);
        c3.setBorder(BorderFactory.createEmptyBorder());
        c3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Character button clicked");
                MapPanel.mode = 111;
            }
        });

        JButton c4 = new JButton(new ImageIcon(Character.getImagePath() + "c1" + ".png"));
        c4.setContentAreaFilled(false);
        c4.setBorder(BorderFactory.createEmptyBorder());
        c4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Character button clicked");
                MapPanel.mode = 1111;
            }
        });

        //////////////////////////////////////////////////////////////////

        JPanel bottom_p = new JPanel() {
            @Override
            protected void paintComponent(Graphics grphcs) {
                super.paintComponent(grphcs);
                grphcs.drawImage(background, 0, 0, this.getWidth(), this.getHeight(), null);
            }
        };
        bottom_p.setBorder(BorderFactory.createLineBorder(Color.BLACK));

        JPanel add_p2 = new JPanel() {
            @Override
            protected void paintComponent(Graphics grphcs) {
                super.paintComponent(grphcs);
                grphcs.drawImage(background, 0, 0, this.getWidth(), this.getHeight(), null);
            }
        };
        add_p2.setLayout(new GridLayout(1, 3, 20, 0));


        JLabel a = new JLabel("Select Action Type: ");
        JRadioButton moveButton = new JRadioButton("Move");
        moveButton.setOpaque(false);
        moveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MapPanel.selectedActionType = 1;
            }
        });

        JRadioButton attackButton = new JRadioButton("Attack");
        attackButton.setOpaque(false);
        attackButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MapPanel.selectedActionType = 2;
            }
        });

        JRadioButton selectButton = new JRadioButton("Select");
        selectButton.setOpaque(false);
        selectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MapPanel.selectedActionType = 3;
            }
        });
        JLabel whoseTurn = new JLabel("1st player's turn");
        JButton endTurn = new JButton("End turn");
        endTurn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (turn == 0) {
                    whoseTurn.setText("2nd player's turn");
                    turn = 1;
                    MapPanel.turn = 1;
                } else if (turn == 1) {
                    whoseTurn.setText("1st player's turn");
                    turn = 0;
                    MapPanel.turn = 0;
                }

            }
        });


        ButtonGroup group = new ButtonGroup();
        group.add(moveButton);
        group.add(attackButton);
        group.add(selectButton);


        add_p.add(b1);
        add_p.add(b2);
        add_p.add(b3);
        add_p.add(b4);
        add_p.add(c1);
        add_p.add(c2);
        add_p.add(c3);
        add_p.add(c4);

        add_p2.add(whoseTurn);
        add_p2.add(endTurn);
        add_p2.add(a);
        add_p2.add(moveButton);
        add_p2.add(attackButton);
        add_p2.add(selectButton);


        side_p.add(add_p);
        bottom_p.add(add_p2);

        this.add(mapPanel, BorderLayout.CENTER);
        this.add(top_p, BorderLayout.NORTH);
        this.add(side_p, BorderLayout.WEST);
        this.add(bottom_p, BorderLayout.SOUTH);


        setVisible(true);

    }


}