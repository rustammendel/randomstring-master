package tester;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;

public class MapPanel extends JPanel {

    public static int mode = 0;
    public static Character selected_char;
    public static Building selectedBuilding;
    public static int selected_build_type;
    public static int selectedActionType;
    public static String imagePath = "src/resources/map/";
    public static int turn = 0;
    public static Player player1;
    public static Player player2;
    public final ArrayList<Player> players = new ArrayList<>();
    public int representationSize;
    private final Image background;
    private final Character c;
    private final Character c1;
    private final ImageIcon imgicn;
    private Image bimg;
    private final JLabel backgnd;


    public MapPanel() {
        background = new ImageIcon(imagePath + "/paper.png").getImage();

        player1 = new Player(1);
        player2 = new Player(2);
        players.add(player1);
        players.add(player2);
        representationSize = 0;
        for (int i = 0; i < players.size(); i++) {
            representationSize += players.get(i).getRepresentationSize();
        }


        bimg = null;
        try {
            bimg = ImageIO.read(new File(imagePath + "grid_new.png"));
        } catch (IOException e) {
            System.out.println("error");
        }
        imgicn = new ImageIcon(bimg);
        backgnd = new JLabel();
        try {
            backgnd.setIcon(imgicn);
            backgnd.setBounds(0, 0, 640, 480);
        } catch (Exception ex) {
            System.out.println(ex);
        }

        this.setLayout(null);// set layout for map


        ////////////////////////////////////////////////GENERATING TEST////////////////////////////////////////////////

        startingGame();

        Point buildingPoint = new Point(2, 8);
        Building building = new Building(0, 2, buildingPoint);
        player1.addBuilding(building);
        add(building.getButton());

        Point characterPoint = new Point(4, 9);


        Point point = new Point(7, 9);
        Point secondPoint = new Point(17, 9);
        c = new Character(0, point, 0);
        c.setDamageLevel(25);
        add(c.button);
        add(c.getHealthBar().getButton(), c.getHealthBar().getPoint());
        c1 = new Character(1, secondPoint, 0);
        c1.setDamageLevel(25);
        add(c1.button);
        add(c1.getHealthBar().getButton(), c.getHealthBar().getPoint());
        player1.addCharacter(c);
        player1.setSoldierLabel();
        player2.addCharacter(c1);


        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                Point newPoint = new Point();
                Point mousePoint = new Point();

                mousePoint.x = e.getX() / 40;
                mousePoint.y = e.getY() / 40;

                newPoint.x = e.getX() / 40;
                newPoint.y = e.getY() / 40;

                System.out.println("Mouse point x: " + mousePoint.x + "              Mouse point y: " + mousePoint.y);
                if (selectedActionType == 1) {
                    if (selected_char.getId() == turn) {
                        selected_char.move(newPoint);
                        selected_char.getHealthBar().setPoint(newPoint);
                    } else {
                        System.out.println("This character doesn't belong you, therefore you can't move it!");
                        System.out.println("Characterin id'i: " + selected_char.getId() + ", turn: " + turn);
                    }
                } else if (selectedActionType == 3) {
                    if (mode == 1) {
                        System.out.println("mode1");
                        if (getPlayerByTurn().getCoins() > 500) {
                            Character ca = new Character(turn, newPoint, 500);
                            ca.setDamageLevel(25);
                            add(ca.button);
                            add(ca.getHealthBar().getButton(), ca.getHealthBar().getPoint());
                            getPlayerByTurn().addCharacter(ca);
                            getPlayerByTurn().setSoldierLabel();
                        }

                        revalidate();
                        repaint();
                        setVisible(true);
                    } else if (mode == 11) {
                        System.out.println("mode11");
                        if (getPlayerByTurn().getCoins() > 500) {

                            Character ca = new Character(turn, characterPoint, 500);
                            ca.setDamageLevel(50);
                            add(ca.getHealthBar().getButton(), ca.getHealthBar().getPoint());
                            add(ca.button);
                            getPlayerByTurn().addCharacter(ca);
                            getPlayerByTurn().setSoldierLabel();
                        }

                        revalidate();
                        repaint();
                        setVisible(true);
                    } else if (mode == 111) {
                        System.out.println("mode111");
                        if (getPlayerByTurn().getCoins() > 500) {

                            Character ca = new Character(turn, characterPoint, 500);
                            ca.setDamageLevel(75);
                            add(ca.getHealthBar().getButton(), ca.getHealthBar().getPoint());
                            add(ca.button);
                            getPlayerByTurn().addCharacter(ca);
                            getPlayerByTurn().setSoldierLabel();
                        }

                        revalidate();
                        repaint();
                        setVisible(true);
                    }

                    if (mode == 1111) {
                        System.out.println("mode1111");
                        if (getPlayerByTurn().getCoins() > 500) {

                            Character ca = new Character(turn, characterPoint, 500);
                            ca.setDamageLevel(100);
                            add(ca.getHealthBar().getButton(), ca.getHealthBar().getPoint());
                            add(ca.button);
                            getPlayerByTurn().addCharacter(ca);
                            getPlayerByTurn().setSoldierLabel();
                        }

                        revalidate();
                        repaint();
                        setVisible(true);
                    } else if (mode == 2) {
                        System.out.println("mode2");
                        if (selected_build_type == 4) {
                            if (
                                    (GameEngine.runtime_obstacle_list[mousePoint.y * GameEngine.n_blocks_in_row + mousePoint.x] == 0) &&
                                            (GameEngine.obstacle_list[mousePoint.y * GameEngine.n_blocks_in_row + mousePoint.x] == 0) &&
                                            (GameEngine.runtime_obstacle_list[(mousePoint.y + 1) * GameEngine.n_blocks_in_row + mousePoint.x] == 0) &&
                                            (GameEngine.obstacle_list[(mousePoint.y + 1) * GameEngine.n_blocks_in_row + mousePoint.x] == 0) &&
                                            (GameEngine.runtime_obstacle_list[(mousePoint.y + 2) * GameEngine.n_blocks_in_row + mousePoint.x] == 0) &&
                                            (GameEngine.obstacle_list[(mousePoint.y + 2) * GameEngine.n_blocks_in_row + mousePoint.x] == 0)
                            ) {
                                if (getPlayerByTurn().getCoins() > 5000) {
                                    Building b = new Building(5000, MapPanel.selected_build_type, mousePoint);
                                    add(b.getButton());
                                    getPlayerByTurn().addBuilding(b);
                                }

                            }
                        } else if (
                                (GameEngine.runtime_obstacle_list[mousePoint.y * GameEngine.n_blocks_in_row + mousePoint.x] == 0) &&
                                        (GameEngine.obstacle_list[mousePoint.y * GameEngine.n_blocks_in_row + mousePoint.x] == 0) &&
                                        (GameEngine.runtime_obstacle_list[(mousePoint.y + 1) * GameEngine.n_blocks_in_row + mousePoint.x] == 0) &&
                                        (GameEngine.obstacle_list[(mousePoint.y + 1) * GameEngine.n_blocks_in_row + mousePoint.x] == 0) &&
                                        (GameEngine.runtime_obstacle_list[(mousePoint.y + 1) * GameEngine.n_blocks_in_row + (mousePoint.x + 1)] == 0) &&
                                        (GameEngine.obstacle_list[(mousePoint.y + 1) * GameEngine.n_blocks_in_row + (mousePoint.x + 1)] == 0) &&
                                        (GameEngine.runtime_obstacle_list[(mousePoint.y) * GameEngine.n_blocks_in_row + (mousePoint.x + 1)] == 0) &&
                                        (GameEngine.obstacle_list[(mousePoint.y) * GameEngine.n_blocks_in_row + (mousePoint.x + 1)] == 0)
                        ) {
                            if (getPlayerByTurn().getCoins() > 5000 && MapPanel.selected_build_type == 3) {
                                Mine m = new Mine(5000, MapPanel.selected_build_type, mousePoint);
                                add(m.getButton());
                                getPlayerByTurn().addBuilding(m);
                            } else if (getPlayerByTurn().getCoins() > 3000 && MapPanel.selected_build_type != 1) {
                                Building b = new Building(3000, MapPanel.selected_build_type, mousePoint);
                                add(b.getButton());
                                getPlayerByTurn().addBuilding(b);
                            }
                        }
                        getPlayerByTurn().setBuildingLabel();
                    }


                    revalidate();
                    repaint();
                    setVisible(true);
                }
            }

        });


        setVisible(true);
        repaint();

    }

    public static Player getPlayerByTurn() {
        if (turn == 0) {
            return player1;
        } else {
            return player2;
        }
    }

    public static void pointChanger(Point point, int x, int y) {
        point.x = x;
        point.y = y;
    }

    public byte[] toByteArray() {
        int numBytes = 0;
        for (int i = 0; i < players.size(); i++) {
            numBytes += players.get(i).getRepresentationSize();
        }
        ByteBuffer bb = ByteBuffer.allocate(numBytes);
        for (int i = 0; i < players.size(); i++) {
            bb.put(players.get(i).toByteArray());
        }
        return bb.array();
    }

    public final synchronized void fromByteArray(byte[] array) {
        if (array == null || array.length != representationSize) return;
        ByteArrayInputStream bais = new ByteArrayInputStream(array);
        try {
            for (int i = 0; i < players.size(); i++) {
                players.get(i).updateFromStream(bais);
            }
            int byteIndex = 0;
            for (int i = 0; i < players.size(); i++) {
                byteIndex += players.get(i).getRepresentationSize();
            }

        } catch (IOException e) {
        }
    }

    @Override
    protected void paintComponent(Graphics g) {

        super.paintComponent(g);
        g.drawImage(background, 0, 0, this.getWidth(), this.getHeight(), null);
        g.drawImage(bimg, 0, 0, null);
    }

    public void startingGame() {
        Point point = new Point(2, 5);
        Point secondPoint = new Point(21, 5);

        Building headquarter = new Building(0, 1, point);
        headquarter.setHeadquarterLife(100);
        player1.addBuilding(headquarter);
        add(headquarter.getButton());
        add(headquarter.getHealthBar().getButton(), headquarter.getHealthBar().getPoint());


        pointChanger(point, 2, 11);
        Mine mine = new Mine(0, 3, point);
        player1.addBuilding(mine);
        mine.dunyaGedir();
        add(mine.getButton());

        pointChanger(point, 7, 4);
        Building watchTower = new Building(0, 4, point);
        player1.addBuilding(watchTower);
        add(watchTower.getButton());

        pointChanger(point, 7, 12);
        Building watchTower1 = new Building(0, 4, point);
        player1.addBuilding(watchTower1);
        add(watchTower1.getButton());


        ////////////////////////////////////////////////////////////////////////

        pointChanger(secondPoint, 21, 5);
        Building headquarter2 = new Building(0, 1, secondPoint);
        player2.addBuilding(headquarter2);
        headquarter2.setHeadquarterLife(100);
        add(headquarter2.getButton());
        add(headquarter2.getHealthBar().getButton(), headquarter2.getHealthBar().getPoint());

        pointChanger(secondPoint, 21, 8);
        Building building2 = new Building(0, 2, secondPoint);
        player2.addBuilding(building2);
        add(building2.getButton());

        pointChanger(secondPoint, 21, 11);
        Building mine2 = new Building(0, 3, secondPoint);
        player2.addBuilding(mine2);
        add(mine2.getButton());

        pointChanger(secondPoint, 17, 4);
        Building watchTower2 = new Building(0, 4, secondPoint);
        player2.addBuilding(watchTower2);
        add(watchTower2.getButton());

        pointChanger(secondPoint, 17, 12);
        Building watchTower21 = new Building(0, 4, secondPoint);
        player2.addBuilding(watchTower21);
        add(watchTower21.getButton());

        repaint();
        setVisible(true);
        player1.setBuildingLabel();
    }

}
