package tester;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class Character extends Item implements ActionListener {
    //Visual parameters:
    public static ArrayList<Character> characters = new ArrayList<>();
    private static final String imagePath = "src/resources/character/";
    private static final String healthBarImagePath = "src/resources/character/healthBar/";
    //Properties:
    private static int count = 0;
    private final int total_frames = 8;
    //
    public String characterType;
    public Point coordOfChar;
    public int damageLevel;
    public Point coordOfHealthButton;
    private final ImageIcon[][] images;
    private int current_frame = 0;
    private final int button_dim = 40;
    private int life;
    private int direction = 0;
    private final int block_size = 40;
    //
    private ArrayList<Point> path; //No need to transmit
    private final int ID; //For debug only
    private final HealthBar healthBar;
    private Timer t;
    private Point vector;
    private Point apath;
    private final GameEngine ge = new GameEngine();

    //CONSTRUCTOR

    public Character(int ID, Point xp, int price) {
        super(ID, xp, price);
        life = 100;
        count = count + 1;
        this.ID = count;
        healthBar = new HealthBar();
        coordOfChar = new Point();
        coordOfHealthButton = new Point();

        coordOfChar.x = xp.x * block_size;
        coordOfChar.y = xp.y * block_size;

        coordOfHealthButton.x = coordOfChar.x;
        coordOfHealthButton.y = (coordOfChar.y - 1);

        button.setOpaque(false);
        button.setContentAreaFilled(false);
        button.setBorderPainted(false);
        button.setLayout(null);
        button.setBounds(coordOfChar.x, coordOfChar.y, button_dim, button_dim);
        button.addActionListener(this::actionPerformed);

        this.images = new ImageIcon[total_frames][total_frames];
        for (int i = 0; i < total_frames; i++) {
            for (int j = 0; j < total_frames; ++j) {
                images[i][j] = new ImageIcon(imagePath + (i * total_frames + j) + ".png");
            }
        }

        button.setIcon(images[direction][current_frame]);
        try {
            button.setIcon(images[0][0]);
        } catch (Exception ex) {
            System.out.println(ex);
        }

        healthBar.button.setBounds(coordOfHealthButton.x, coordOfHealthButton.y, 40, 7);

    }

    //METHODS

    public static void addToFightingCharacterList(Character character) {
        characters.add(character);
        System.out.println("You added this character to the list: " + character.getP());
    }

    //Get/Setters
    public static String getImagePath() {
        return imagePath;
    }

    public void actionPerformed(ActionEvent e) {
        MapPanel.mode = 0;
        MapPanel.selected_char = this;
        System.out.println("You selected character");
        if (MapPanel.selectedActionType == 2) {
            addToFightingCharacterList(this);
            if (characters.get(0).getId() != MapPanel.turn) {
                characters.remove(0);
            }
            if (characters.size() == 2) {
                if (characters.get(0).getId() != characters.get(1).getId()) {
                    characters.get(0).attack(characters.get(1));
                } else {
                    characters.remove(1);
                    System.out.println("Sapı özümüzdən olan baltalardan olma!!!");
                }
            }
        }
    }

    public void attack(Character character) {
        int difference = (int) (character.getP().getX() - getP().getX());
        if (difference != 2 && difference != -2) {

            move(getMoveablePoint(character));
        }
        int time = (int) (625 * (character.getP().getX() - 2 - getP().getX()));
        new java.util.Timer().schedule(
                new java.util.TimerTask() {
                    @Override
                    public void run() {
                        character.life -= damageLevel;
                        character.setHealthBarImage();
                    }
                },
                time
        );

        characters.remove(1);
        characters.remove(0);
        System.out.println(System.currentTimeMillis());
    }

    public void attack(Building building) {
        building.setHeadquarterLife(building.getHeadquarterLife() - 20);
    }

    public void move(Point ep) {

        Point tempp = new Point(ep.x, ep.y);

        System.out.println(ID + "   Function called with parameters " + ep.x + " " + ep.y);

        path = ge.getPath(this.getP(), ep);
        apath = new Point(path.get(0).x, path.get(0).y);

        System.out.println(ID + "    NEXT ONE MOVE " + apath.x + " " + apath.y);
        vector = new Point(0, 0);

        vector.x = apath.x - getP().x;
        vector.y = apath.y - getP().y;

        //
        if (vector.x == 0 && vector.y == -1) {
            direction = 3;
        } else if (vector.x == 1 && vector.y == -1) {
            direction = 7;
        } else if (vector.x == 1 && vector.y == 0) {
            direction = 2;
        } else if (vector.x == 1 && vector.y == 1) {
            direction = 6;
        } else if (vector.x == 0 && vector.y == 1) {
            direction = 0;
        } else if (vector.x == -1 && vector.y == 1) {
            direction = 4;
        } else if (vector.x == -1 && vector.y == 0) {
            direction = 1;
        } else {
            direction = 5;
        }
        System.out.println("Timer started");
        t = new Timer(10, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if ((coordOfChar.x != (apath.x * block_size) || (coordOfChar.y != (apath.y) * block_size))) {
                    coordOfChar.x = coordOfChar.x + vector.x;
                    coordOfChar.y = coordOfChar.y + vector.y;
                    button.setBounds(coordOfChar.x, coordOfChar.y, block_size, block_size);
                    coordOfHealthButton.x = coordOfChar.x;
                    coordOfHealthButton.y = (coordOfChar.y - 1);
                    healthBar.button.setBounds(coordOfHealthButton.x, coordOfHealthButton.y, 40, 7);
                    current_frame++;
                    button.setIcon(images[direction][current_frame % 8]);


                } else {
                    p.x = apath.x;
                    p.y = apath.y;
                    t.stop();
                    if (!(p.x == ep.x && p.y == ep.y)) {
                        move(tempp);
                    }
                }
            }


        });

        t.start();

    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public int getLife() {
        return life;
    }

    public void setLife(int life) {
        this.life = life;
    }

    public Character getCharacter(Point point) {
        if (this.getP() == point) {
            return this;
        }
        return null;
    }

    public Point getMoveablePoint(Character character) {
        Point point;
        if (character.getP().getX() > getP().getX()) {
            point = new Point((int) character.getP().getX() - 2, (int) character.getP().getY());
            return point;
        } else {
            point = new Point((int) character.getP().getX() + 2, (int) character.getP().getY());
            return point;
        }
    }

    public HealthBar getHealthBar() {
        return healthBar;
    }

    public void setHealthBarImage() {
        if (life == 75) {
            ImageIcon imageIcon = new ImageIcon(healthBarImagePath + 75 + ".png");
            healthBar.getButton().setIcon(imageIcon);
        } else if (life == 50) {
            ImageIcon imageIcon = new ImageIcon(healthBarImagePath + 50 + ".png");
            healthBar.getButton().setIcon(imageIcon);
        } else if (life == 25) {
            ImageIcon imageIcon = new ImageIcon(healthBarImagePath + 25 + ".png");
            healthBar.getButton().setIcon(imageIcon);
        } else if (life == 0) {
            ImageIcon imageIcon = new ImageIcon(healthBarImagePath + 0 + ".png");
            healthBar.getButton().setIcon(imageIcon);
        }

        System.out.println("Qaqa gorursen rengi basdim ee");
    }

    public int getDamageLevel() {
        return damageLevel;
    }

    public void setDamageLevel(int damageLevel) {
        this.damageLevel = damageLevel;
    }
}