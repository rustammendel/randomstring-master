package tester;

import networking.ClientDisplay;
import networking.GameBoardNetClient;
import networking.GameBoardNetServer;
import networking.ServerDisplay;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Window extends JFrame implements KeyListener, ServerDisplay, ClientDisplay {

    public static CardLayout cl;
    public static JPanel windows;
    private final GameBoardNetServer server;
    private final GameBoardNetClient client;
    public GameWindow gameWindow;
    public JPanel mainWindow;
    public JPanel settingsWindow;
    public JPanel aboutWindow;
    public JPanel newGameWindow;
    public JPanel connectServerPanel;
    public JPanel createServerPanel;

    public JLabel mainmenuText;
    public JButton newGameButton;
    public JButton settingsButton;
    public JButton AboutButton;
    public JButton quitButton;
    public JButton backtogameButton;
    public JButton backButton;

    public JTextField textAddress;
    public JTextField textPort;

    public JLabel t5;
    public JButton b;
    public String pathToImage = "src/resources/map/";
    public Image background = new ImageIcon(pathToImage + "/paper.png").getImage();
    public String buttonImg = pathToImage + "/style.png";

    private GameMode gameMode = GameMode.LOCAL;


    //CONSTRUCTORS
    public Window() throws Exception {
        server = new GameBoardNetServer(25, 5000);
        client = new GameBoardNetClient();
        /*server.setDisplay(this);
        //timer.setRepeats(true);
        // server.generateMap();
        //client.initGame();
        //  gamePanel.setGameBoard(client);
        server.start();
        client.setDisplay(this);
        // client.setServerAddress("127.0.0.1", getPort());
        client.start();
*/
/*
        if (gameMode == GameMode.LOCAL){
            setTitle("RTS - Two players!");
            localGame.initGame();
            gamePanel.setGameBoard(localGame);
            timer.start();
        } else if (gameMode == GameMode.SERVER){
            setTitle("RTS - Multiplayer (host)");
            server.initGame();
            client.initGame();
            gamePanel.setGameBoard(client);
            server.start();
            client.setDisplay(this);
            client.setServerAddress("127.0.0.1", getPort());
            client.start();
            //int reason = progressDialog.showDialog(0);
            //System.out.println("Reason: " + reason);
        } else {
            setTitle("RTS - Multiplayer (guest)");
            client.initGame();
            gamePanel.setGameBoard(client);
            client.setDisplay(this);
            client.setServerAddress(getAddress(),getPort());
            client.start();
        }*/


        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        setTitle("Window");
        setSize(800, 600); //TODO: automatically set window size according to tile size and count

        //panel to contain list of panels and make it card layout
        windows = new JPanel();
        windows.setLayout(new CardLayout());
        cl = (CardLayout) (windows.getLayout());

        //--//--------------Main Window ------------------
        mainWindow = new JPanel() {
            @Override
            protected void paintComponent(Graphics grphcs) {
                super.paintComponent(grphcs);
                grphcs.drawImage(background, 0, 0, this.getWidth(), this.getHeight(), null);
            }
        };

        mainWindow.setLayout(new BoxLayout(mainWindow, BoxLayout.Y_AXIS));
        mainmenuText = new JLabel("MAIN MENU");
        mainmenuText.setAlignmentX(Component.CENTER_ALIGNMENT);


        newGameButton = new JButton("New Game", new ImageIcon(buttonImg));
        buttonDesigner(newGameButton);
        newGameButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                newGameWindowConstructor();
                cl.show(windows, "newGameWindow");
            }
        });

        settingsButton = new JButton("Settings", new ImageIcon(buttonImg));
        buttonDesigner(settingsButton);

        AboutButton = new JButton("About", new ImageIcon(buttonImg));
        buttonDesigner(AboutButton);
        AboutButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                cl.show(windows, "aboutWindow");
            }
        });

        quitButton = new JButton("Quit", new ImageIcon(buttonImg));
        buttonDesigner(quitButton);
        quitButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                closeApp();
            }
        });

        backtogameButton = new JButton("BACK TO GAME", new ImageIcon(buttonImg));
        buttonDesigner(backtogameButton);

        backtogameButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cl.show(windows, "gameWindow");
            }
        });
        mainWindow.add(mainmenuText);
        mainWindow.add(newGameButton);
        mainWindow.add(settingsButton);
        mainWindow.add(AboutButton);
        mainWindow.add(quitButton);
        mainWindow.add(backtogameButton);

        //-----------------GAME WINDOW-----------------------------------
        gameWindow = new GameWindow();


        //-----------------NEW GAME WINDOW-------------------------------


        //----------------Connect to server -----------------------------
        createServerPanel = new JPanel() {
            @Override
            protected void paintComponent(Graphics grphcs) {
                super.paintComponent(grphcs);
                grphcs.drawImage(background, 0, 0, this.getWidth(), this.getHeight(), null);
            }
        };
        createServerPanel.setLayout(new BoxLayout(createServerPanel, BoxLayout.Y_AXIS));
        //----------------Create server window --------------------------
        //TODO: text: waiting for clients...
        //Todo: print ip and port -> textbox

        //----------------SETTINGS WINDOW--------------------------------
        settingsWindow = new JPanel();

        //----------------ABOUT WINDOW-----------------------------------

        aboutWindow = new JPanel() {
            @Override
            protected void paintComponent(Graphics grphcs) {
                super.paintComponent(grphcs);
                grphcs.drawImage(background, 0, 0, this.getWidth(), this.getHeight(), null);
            }
        };
        aboutWindow.setLayout(new BoxLayout(aboutWindow, BoxLayout.Y_AXIS));
        t5 = new JLabel("ABOUT");

        t5.setAlignmentX(Component.CENTER_ALIGNMENT);

        b = new JButton("BACK TO MAIN MENU", new ImageIcon(buttonImg));
        buttonDesigner(b);
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cl.show(windows, "mainWindow");
            }
        });
        aboutWindow.add(t5);
        aboutWindow.add(b);

        //---------------------------------------------------------------

        windows.add(mainWindow, "mainWindow");
        windows.add(gameWindow, "gameWindow");
        //Todo reorganisation needed

        windows.add(settingsWindow);
        windows.add(aboutWindow, "aboutWindow");


        this.add(windows);
        setVisible(true);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }

    public static void main(String[] arguments) throws Exception {
        Window w = new Window();

    }

    public static CardLayout getCL() {
        return cl;
    }

    public static JPanel getWindow() {
        return windows;
    }

    public void buttonDesigner(JButton button) {
        button.setHorizontalTextPosition(JButton.CENTER);
        button.setAlignmentX(Component.CENTER_ALIGNMENT);
        button.setBorder(BorderFactory.createEmptyBorder());
        button.setContentAreaFilled(false);
        button.setForeground(Color.lightGray);
    }

    public void newGameWindowConstructor() {

        newGameWindow = new JPanel() {
            @Override
            protected void paintComponent(Graphics grphcs) {
                super.paintComponent(grphcs);
                grphcs.drawImage(background, 0, 0, this.getWidth(), this.getHeight(), null);
            }
        };

        newGameWindow.setLayout(new BoxLayout(newGameWindow, BoxLayout.Y_AXIS));


        JButton newGameButton = new JButton("Single Player game", new ImageIcon(buttonImg));
        buttonDesigner(newGameButton);

        JButton createServerButton = new JButton("Create new server", new ImageIcon(buttonImg));
        buttonDesigner(createServerButton);

        JButton connectToServerButton = new JButton("Connect to server", new ImageIcon(buttonImg));
        buttonDesigner(connectToServerButton);
        connectToServerPanelConstructor();


        backButton = new JButton("BACK TO MAIN MENU", new ImageIcon(buttonImg));
        buttonDesigner(backButton);
        //Action listeners

        newGameButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //todo bura qoy
                // gameWindow = new GameWindow();
                cl.show(windows, "gameWindow");
            }
        });

        createServerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                createServerPanelConstructor();
                cl.show(windows, "createServerPanel");


            }
        });

        connectToServerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                connectToServerPanelConstructor();
                cl.show(windows, "connectServerPanel");
            }
        });


        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cl.show(windows, "mainWindow");
            }
        });


        newGameWindow.add(newGameButton, "newGame");
        newGameWindow.add(createServerButton);
        newGameWindow.add(connectToServerButton);
        newGameWindow.add(backButton);

        windows.add(newGameWindow, "newGameWindow");

    }

    public void createServerPanelConstructor() {
        createServerPanel = new JPanel() {
            @Override
            protected void paintComponent(Graphics grphcs) {
                super.paintComponent(grphcs);
                grphcs.drawImage(background, 0, 0, this.getWidth(), this.getHeight(), null);
            }
        };

        createServerPanel.setLayout(new GridBagLayout());
        //TODO: complete below
        //text: ip adress
        // text:port number
        //button: create ->

        //Buttons

        backButton = new JButton("BACK TO MAIN MENU", new ImageIcon(buttonImg));
        buttonDesigner(backButton);

        JButton startServerButton = new JButton("Start Server", new ImageIcon(buttonImg));
        buttonDesigner(startServerButton);

        //Action listeners

        startServerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //Todo:
                //Texts to parameters
                System.out.println("Game mode set to server");

                gameMode = GameMode.SERVER;

            }
        });

        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cl.show(windows, "mainWindow");
            }
        });

        createServerPanel.add(startServerButton);
        createServerPanel.add(backButton);

        windows.add(createServerPanel, "createServerPanel");

    }

    public void connectToServerPanelConstructor() {
        connectServerPanel = new JPanel() {
            @Override
            protected void paintComponent(Graphics grphcs) {
                super.paintComponent(grphcs);
                grphcs.drawImage(background, 0, 0, this.getWidth(), this.getHeight(), null);
            }
        };

        connectServerPanel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        Font font1 = new Font("SansSerif", Font.BOLD, 20);
        //--------- IP adress input -------------
        JLabel labelAddress = new JLabel("Enter the IP adress: ");
        textAddress = new JTextField();
        textAddress.setForeground(Color.BLACK);
        textAddress.setBackground(Color.lightGray);
        textAddress.setBorder(BorderFactory.createLineBorder(Color.black));
        textAddress.setFont(font1);
        textAddress.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                if (textAddress.getText().length() >= 12)
                    e.consume();
            }
        });
        //----------------------------------------
        //-------- Port number input -------------
        JLabel labelPort = new JLabel("Enter the port: ");
        textPort = new JTextField();
        textPort.setForeground(Color.BLACK);
        textPort.setBackground(Color.LIGHT_GRAY);
        textPort.setBorder(BorderFactory.createLineBorder(Color.black));
        textPort.setFont(font1);
        textPort.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                if (textPort.getText().length() >= 6)
                    e.consume();
            }
        });
        //----------------------------------------

        //Buttons
        JButton connect = new JButton("CONNECT ", new ImageIcon(buttonImg));
        buttonDesigner(connect);

        backButton = new JButton("BACK TO MAIN MENU", new ImageIcon(buttonImg));
        buttonDesigner(backButton);

        //Action listeners
        connect.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Your port number: " + textPort.getText());
                System.out.println("Your IP adress: " + textAddress.getText());
                gameMode = GameMode.CLIENT;
                System.out.println("Game mode set to client");

            }
        });
        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cl.show(windows, "mainWindow");
            }
        });


        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 0;
        connectServerPanel.add(labelAddress, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 1;
        c.gridy = 0;
        c.gridwidth = 5;
        connectServerPanel.add(textAddress, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 1;
        connectServerPanel.add(labelPort, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 1;
        c.gridy = 1;
        connectServerPanel.add(textPort, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 2;
        connectServerPanel.add(connect, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 3;
        connectServerPanel.add(backButton, c);

        windows.add(connectServerPanel, "connectServerPanel");

    }

    //METHODS
    public String getAddress() {
        return textAddress.getText();
    }

    public int getPort() {
        return Integer.parseInt(textPort.getText());
    }

    void closeApp() {
        this.dispose();
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void refresh() {

    }

    @Override
    public void connectionLost() {

    }

    @Override
    public void cannotConnect() {

    }

    @Override
    public void connectionLost(int client) {

    }

    @Override
    public void cannotOpenServer() {

    }

    @Override
    public void allClientsAreConnected() {

    }
}