package tester;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class HealthBar implements ActionListener {
    protected JButton button;
    private Point point;
    private ImageIcon imageIcon;
    private final int buttomDimension = 40;
    private Point pixel;

    public HealthBar() {
        button = new JButton();
        button.setOpaque(false);
        button.setContentAreaFilled(false);
        button.setBorderPainted(false);
        button.setLayout(null);
        button.addActionListener(this::actionPerformed);
    }

    public Point getPoint() {
        return point;
    }

    public void setPoint(Point point) {
        this.point = point;
    }

    public ImageIcon getImageIcon() {
        return imageIcon;
    }

    public void setImageIcon(ImageIcon imageIcon) {
        this.imageIcon = imageIcon;
    }

    public JButton getButton() {
        return button;
    }

    public void setButton(JButton button) {
        this.button = button;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("Qaqa healthBara toxundun");
    }

    public void setButtonBounds(int x, int y, int width, int height) {
        button.setBounds(x, y, width, height);
    }
}
