package tester;


import javax.swing.*;
import java.awt.*;

public abstract class Item {
    protected int id;
    protected Point p;
    protected int price;
    protected JButton button;

    public Item(int id, Point p, int price) {
        this.id = id;
        this.p = p;
        this.price = price;
        this.button = new JButton();
    }

    public int getId() {
        return id;
    }

    public Point getP() {
        return p;
    }

    public int getPrice() {
        return price;
    }

    public JButton getButton() {
        return button;
    }

}
