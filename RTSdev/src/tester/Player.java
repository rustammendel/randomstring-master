package tester;

import java.awt.*;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.ArrayList;

public class Player implements Serializable {

    public int representationSize;
    //fields
    private int playerId;
    private ArrayList<Character> characters;
    private ArrayList<Building> buildings;
    private int coins;

    //constructors
    public Player(int ID) {
        playerId = ID;
        characters = new ArrayList<>();
        buildings = new ArrayList<>();
        coins = 1000;
        representationSize = 1 + (this.buildings.size() * 3) + 1 + (this.characters.size() * 4) + 1;
        setLabel();
    }

    public int getRepresentationSize() {
        return 1 + (this.buildings.size() * 3) + 1 + (this.characters.size() * 4) + 1;
    }

    //methods

    //network
    public void updateFromStream(InputStream is) throws IOException {
        // Representation requires 1 + 2 + 1 + 2 + 1 = 6 bytes
        // Mappinng: numOfBuilds + Builds.x + Builds.y ...
        DataInputStream dis = new DataInputStream(is);
        //building
        short numOfBuilds = dis.readShort();
        for (int i = 0; i < numOfBuilds; i++) {
            getBuildings().get(i).p.x = dis.readShort();
            getBuildings().get(i).p.y = dis.readShort();
            getBuildings().get(i).setBuildingType(dis.readShort()); //different setup
        }
        //charachter
        short numOfChar = dis.readShort();
        for (int i = 0; i < numOfChar; i++) {
            getCharacters().get(i).p.x = dis.readShort();
            getCharacters().get(i).p.y = dis.readShort();
            getCharacters().get(i).setDirection(dis.readShort());
            getCharacters().get(i).setLife(dis.readShort());
        }
        setCoins(dis.readInt());
    }

    public byte[] toByteArray() {
        // Representation requires 1 + 2 + 1 + 2 + 1 = 6 bytes
        // May write instead into a DataOutputStream(new ByteArrayOutputStream())
        ByteBuffer pbb = ByteBuffer.allocate(100);
        //Players buildings : 0x0 = number of buildings, 0x1 - 0x2 = x,y
        short numOfBuild = (short) getBuildings().size();

        pbb.putShort(numOfBuild);
        for (int i = 0; i < numOfBuild; i++) {
            pbb.putShort((short) getBuildings().get(i).getP().x);
            pbb.putShort((short) getBuildings().get(i).getP().y);
            pbb.putShort((short) getBuildings().get(i).getBuildingType());

        }
        //adding charachters
        // mapping: 1 number of char 2 location 1 direction
        short numOfChar = (short) getCharacters().size();
        pbb.putShort(numOfChar);
        for (int i = 0; i < numOfChar; i++) {
            pbb.putShort((short) getCharacters().get(i).getP().x);
            pbb.putShort((short) getCharacters().get(i).getP().y);
            pbb.putShort((short) getCharacters().get(i).getDirection());
            pbb.putShort((short) getCharacters().get(i).getLife());

        }
        pbb.putInt(getCoins());

        return pbb.array();
    }


    public int getPlayerId() {
        return playerId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    public ArrayList<Character> getCharacters() {
        return characters;
    }

    public void setCharacters(ArrayList<Character> characters) {
        this.characters = characters;
    }

    public ArrayList<Building> getBuildings() {
        return buildings;
    }

    public void setBuildings(ArrayList<Building> buildings) {
        this.buildings = buildings;
    }

    public int getCoins() {
        return coins;
    }

    public void setCoins(int coins) {
        this.coins = coins;
    }

    public ArrayList getCoinsArray() {
        ArrayList<Integer> coinarr = new ArrayList<>();
        coinarr.add(coins);
        return coinarr;
    }

    public void addBuilding(Building b) {
        if (b.getPrice() < coins) {
            coins -= b.getPrice();
            buildings.add(b);
            setLabel();
        } else
            System.out.println("You don't have enough money to buy!");
    }

    public void addCharacter(Character c) {
        coins -= c.getPrice();
        characters.add(c);
        setLabel();
    }

    public Character getChacterByPoint(Point point) {
        for (Character character : characters) {
            if (character.getP() == point) {
                return character;
            }
        }
        return null;
    }

    public void setLabel() {
        String name = Integer.toString(coins);
        GameWindow.availableCoins.setText(name);
    }

    public void setSoldierLabel() {
        String name = Integer.toString(characters.size());
        GameWindow.availableSoldiers.setText(name);
    }

    public void setBuildingLabel() {
        String name = Integer.toString(buildings.size());
        GameWindow.availableBuildings.setText(name);
    }
}
