package animation_prototype;

import javax.swing.*;

public class Animation_prototype {

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        Whatever w = new Whatever();
        frame.add(w);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500,400);
    }
    
}
