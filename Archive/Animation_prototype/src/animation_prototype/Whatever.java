package animation_prototype;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Whatever extends JPanel implements ActionListener{
//fields
    private final Timer timer;
    private final ImageIcon images[];
    private final int delay=80;
    private final int total_frames=6;
    private int current_frame=0;
//constructor    
   public Whatever(){
       this.images = new ImageIcon[total_frames];
       for(int i=0; i<6; i++) {
         images[i] = new ImageIcon("images/runright_"+i+".png");
       }
       timer = new Timer(delay,this);
       timer.start();
   }
   
    @Override
   public void paintComponent(Graphics g){
       super.paintComponent(g);
       if(current_frame>=6){
           current_frame = 0;
       }
       images[current_frame].paintIcon(this, g, 0, 0);
          current_frame++;
   }

    @Override
    public void actionPerformed(ActionEvent e) {
        repaint();
    }
}
