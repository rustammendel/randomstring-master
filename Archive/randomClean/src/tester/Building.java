package tester;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Building {
    public static int count = 0;
    public boolean life;
    public int btype;
    public ImageIcon image;
    public int[] button_dim_x = {0, 80, 80, 80, 40};
    public int[] button_dim_y = {0, 80, 80, 80, 120};
    public Point p;
    public Point pixel;
    public int block_size = 40;
    public JButton button;
    public static String imagePath = "src\\resources\\buildings\\";

    public Building(int type, Point mp) {

        btype = type;
        count = count + 1;
        p = new Point(mp.x, mp.y);
        pixel = new Point(0, 0);
        pixel.x = p.x * block_size;
        pixel.y = p.y * block_size;

        button = new JButton();
        button.setOpaque(false);
        button.setContentAreaFilled(false);
        button.setBorderPainted(false);
        button.setLayout(null);
        button.setBounds(pixel.x, pixel.y, button_dim_x[btype], button_dim_y[btype]);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("building clicked");
            }
        });

        image = new ImageIcon(this.imagePath + "b" + btype + ".png");

        button.setIcon(image);

        try {
            button.setIcon(image);
        } catch (Exception ex) {
            System.out.println(ex);
        }

        if (type == 4) {
            GameEngine.addObstacle(p);
            GameEngine.addObstacle(new Point(p.x, p.y + 1));
            GameEngine.addObstacle(new Point(p.x, p.y + 2));
        } else {
            GameEngine.addObstacle(p);
            GameEngine.addObstacle(new Point(p.x, p.y + 1));
            GameEngine.addObstacle(new Point(p.x + 1, p.y));
            GameEngine.addObstacle(new Point(p.x + 1, p.y + 1));
        }
    }


}
