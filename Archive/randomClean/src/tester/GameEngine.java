package tester;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import static java.lang.StrictMath.sqrt;

public class GameEngine {

    //fields
    static int block_size = 40;
    static int n_blocks_in_row = 38;
    static int n_blocks_in_column = 24;
    static int n_blocks = n_blocks_in_row * n_blocks_in_column;
    static int[] obstacle_list = new int[n_blocks];//array[vertexnumber] = 0 or 1
    static int[] runtime_obstacle_list = new int[n_blocks];
    double[][] map = new double[n_blocks][n_blocks];

    //constructors
    public GameEngine() {
        generateObstaclelist();
        generateMap();
    }

    public static void addObstacle(Point p) {
        runtime_obstacle_list[(p.y * n_blocks_in_row) + p.x] = 1;
    }

    void generateMap() {

        for (int i = 0; i < n_blocks; ++i) {
            for (int j = 0; j < n_blocks; ++j) {
                if (obstacle_list[j] == 1 || runtime_obstacle_list[j] == 1) {
                    map[i][j] = 0;
                } else {


                    if ((i == 0) || (i == n_blocks_in_row - 1) || (i == n_blocks_in_row * (n_blocks_in_column - 1)) || (i == n_blocks_in_row * n_blocks_in_column - 1)) { //case corner
                        if (i == 0) {
                            if (j == i + 1 || j == i + n_blocks_in_row) {
                                map[i][j] = 1;
                            } else if (j == i + n_blocks_in_row + 1) {
                                map[i][j] = sqrt(2);
                            } else {
                                map[i][j] = 0;
                            }
                        } else if (i == n_blocks_in_row - 1) {
                            if (j == i - 1 || j == i + n_blocks_in_row) {
                                map[i][j] = 1;
                            } else if (j == i + n_blocks_in_row - 1) {
                                map[i][j] = sqrt(2);
                            } else {
                                map[i][j] = 0;
                            }
                        } else if (i == n_blocks_in_row * (n_blocks_in_column - 1)) {
                            if (j == i + 1 || j == i - n_blocks_in_row) {
                                map[i][j] = 1;
                            } else if (j == i - n_blocks_in_row + 1) {
                                map[i][j] = sqrt(2);
                            } else {
                                map[i][j] = 0;
                            }
                        } else {
                            if (j == i - 1 || j == i - n_blocks_in_row) {
                                map[i][j] = 1;
                            } else if (j == i - n_blocks_in_row - 1) {
                                map[i][j] = sqrt(2);
                            } else {
                                map[i][j] = 0;
                            }
                        }
                    } else if ((0 < i && i < n_blocks_in_row) || (n_blocks_in_row * (n_blocks_in_column - 1) < i && i < n_blocks_in_row * n_blocks_in_column - 1) || (i % n_blocks_in_row == 0) || (i % n_blocks_in_row == n_blocks_in_row - 1)) { //case sides
                        if (0 < i && i < n_blocks_in_row) {
                            if (j == i - 1 || j == i + 1 || j == i + n_blocks_in_row) {
                                map[i][j] = 1;
                            } else if (j == i + n_blocks_in_row - 1 || j == i + n_blocks_in_row + 1) {
                                map[i][j] = sqrt(2);
                            } else {
                                map[i][j] = 0;
                            }
                        } else if (n_blocks_in_row * (n_blocks_in_column - 1) < i && i < n_blocks_in_row * n_blocks_in_column - 1) {
                            if (j == i - 1 || j == i + 1 || j == i - n_blocks_in_row) {
                                map[i][j] = 1;
                            } else if (j == i - n_blocks_in_row - 1 || j == i - n_blocks_in_row + 1) {
                                map[i][j] = sqrt(2);
                            } else {
                                map[i][j] = 0;
                            }
                        } else if (i % n_blocks_in_row == 0) {
                            if (j == i + 1 || j == i - n_blocks_in_row || j == i + n_blocks_in_row) {
                                map[i][j] = 1;
                            } else if (j == i - n_blocks_in_row + 1 || j == i + n_blocks_in_row + 1) {
                                map[i][j] = sqrt(2);
                            } else {
                                map[i][j] = 0;
                            }
                        } else {
                            if (j == i - 1 || j == i - n_blocks_in_row || j == i + n_blocks_in_row) {
                                map[i][j] = 1;
                            } else if (j == i - n_blocks_in_row - 1 || j == i + n_blocks_in_row - 1) {
                                map[i][j] = sqrt(2);
                            } else {
                                map[i][j] = 0;
                            }
                        }
                    } else { //case center
                        if (j == i + 1 || j == i - 1 || j == i - n_blocks_in_row || j == i + n_blocks_in_row) {
                            map[i][j] = 1;
                        } else if (j == i - n_blocks_in_row - 1 || j == i - n_blocks_in_row + 1 || j == i + n_blocks_in_row - 1 || j == i + n_blocks_in_row + 1) {
                            map[i][j] = sqrt(2);
                        } else {
                            map[i][j] = 0;
                        }
                    }
                }
            }

        }
    }

    void generateObstaclelist() {

        try {
            Scanner scanner = new Scanner(new File("src\\resources\\map\\input2.txt"));
            scanner.useDelimiter(",");
            int ct = 0;
            while (scanner.hasNextLine()) {
                String tmp = new String(scanner.next());
                obstacle_list[ct] = Integer.parseInt(tmp);
                ++ct;
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    ArrayList<Point> getPath(Point sp, Point ep) {

        int sv = sp.y * n_blocks_in_row + sp.x;
        int ev = ep.y * n_blocks_in_row + ep.x;
        ArrayList<Point> path = new ArrayList<Point>();

        path = new ArrayList<Point>();
        findPath(sv, ev, path);
        return path;


    }

    void findPath(int sv, int ev, ArrayList<Point> path) {


        final int NO_PARENT = -1;
        int nVertices = map[0].length;
        // shortestDistances[i] will hold the shortest distance from src to i
        double[] shortestDistances = new double[nVertices];
        // added[i] will true if vertex i is included in shortest path tree or shortest distance from src to i is finalized
        boolean[] added = new boolean[nVertices];


        // Initialize all distances as INFINITE and added[] as false
        for (int vertexIndex = 0; vertexIndex < nVertices; vertexIndex++) {
            shortestDistances[vertexIndex] = Double.MAX_VALUE;
            added[vertexIndex] = false;
        }

        // Distance of source vertex from itself is always 0
        shortestDistances[sv] = 0;
        // Parent array to store shortest path tree
        int[] parents = new int[nVertices];
        // The starting vertex does not have a parent
        parents[sv] = NO_PARENT;

        // Find shortest path for all vertices
        for (int i = 1; i < nVertices; i++) {
            // Pick the minimum distance vertex from the set of vertices not yet processed. nearestVertex is always equal to startNode in first iteration.
            //WHY NEAREST VERTEX SHOULD BE INITIALIZED TO -1???
            int nearestVertex = 0;
            double shortestDistance = Double.MAX_VALUE;
            for (int vertexIndex = 0;
                 vertexIndex < nVertices;
                 vertexIndex++) {
                if (!added[vertexIndex] && shortestDistances[vertexIndex] < shortestDistance) {
                    nearestVertex = vertexIndex;
                    shortestDistance = shortestDistances[vertexIndex];
                }
            }

            // Mark the picked vertex as processed
            added[nearestVertex] = true;

            // Update dist value of the adjacent vertices of the picked vertex.
            for (int vertexIndex = 0; vertexIndex < nVertices; vertexIndex++) {
                double edgeDistance = map[nearestVertex][vertexIndex];

                if (edgeDistance > 0 && ((shortestDistance + edgeDistance) < shortestDistances[vertexIndex])) {
                    parents[vertexIndex] = nearestVertex;
                    shortestDistances[vertexIndex] = shortestDistance + edgeDistance;
                }
            }
        }
        printPath(path, sv, ev, parents);

    }

    void printPath(ArrayList<Point> path, int sv, int cv, int[] parents) {
        // Base case : Source node has been processed
        if (cv == sv) {
            return;
        }
        printPath(path, sv, parents[cv], parents);
        path.add(new Point(cv % n_blocks_in_row, cv / n_blocks_in_row));
        //System.out.println(cv % n_blocks_in_row + " " + cv / n_blocks_in_row);
        return;
    }
}



