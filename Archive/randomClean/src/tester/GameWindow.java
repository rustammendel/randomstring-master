package tester;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GameWindow extends JPanel {

    public MapPanel mapPanel;

    public GameWindow() {
        setLayout(new BorderLayout());

        JPanel top_p = new JPanel();
        top_p.setLayout(new GridLayout(1, 3, 10, 0));
        top_p.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        JButton menu_b = new JButton("MAIN MENU");
        menu_b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Window.getCL().show(Window.getWindow(), "mainWindow");
            }
        });
        JLabel health_l = new JLabel("Health: ");
        JLabel coins_l = new JLabel("Coins: ");
        JLabel soldiers_l = new JLabel("Soldiers: ");
        JLabel buildings_l = new JLabel("Buildings: ");

        top_p.add(health_l);
        top_p.add(coins_l);
        top_p.add(soldiers_l);
        top_p.add(buildings_l);
        top_p.add(menu_b);

        //Side panel for buildings and character choice

        JPanel side_p = new JPanel();
        side_p.setBorder(BorderFactory.createLineBorder(Color.BLACK));

        JPanel add_p = new JPanel();
        add_p.setLayout(new GridLayout(8, 1, 10, 10));
        add_p.setBorder(BorderFactory.createLineBorder(Color.BLACK));

        JButton b1 = new JButton(new ImageIcon(Building.imagePath + "b1" + ".png"));
        b1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("building 1 button clicked");
                MapPanel.mode = 2;
                MapPanel.selected_build_type = 1;
            }
        });

        JButton b2 = new JButton(new ImageIcon(Building.imagePath + "b2" + ".png"));
        b2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("building button clicked");
                MapPanel.mode = 2;
                MapPanel.selected_build_type = 2;
            }
        });
        JButton b3 = new JButton(new ImageIcon(Building.imagePath + "b3" + ".png"));
        b3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("building button clicked");
                MapPanel.mode = 2;
                MapPanel.selected_build_type = 3;
            }
        });
        JButton b4 = new JButton(new ImageIcon(Building.imagePath + "b4" + ".png"));
        b4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("building button clicked");
                MapPanel.mode = 2;
                MapPanel.selected_build_type = 4;
            }
        });

        JButton c1 = new JButton(new ImageIcon(Character.imagePath + "0" + ".png"));
        c1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("building button clicked");
                MapPanel.mode = 1;
            }
        });


        add_p.add(b1);
        add_p.add(b2);
        add_p.add(b3);
        add_p.add(b4);
        add_p.add(c1);
        side_p.add(add_p);

        mapPanel = new MapPanel();
        this.add(mapPanel, BorderLayout.CENTER);
        this.add(top_p, BorderLayout.NORTH);
        this.add(side_p, BorderLayout.WEST);


        setVisible(true);

    }


}