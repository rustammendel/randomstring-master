package tester;

import java.util.ArrayList;

public class Player {

    //fields
    public int playerId;
    public ArrayList<Character> characters;
    public ArrayList<Building> buildings;
    public int coins;
    public Building temp;

    //constructors
    public Player(int id) {
        playerId = id;
        characters = new ArrayList<Character>();
        buildings = new ArrayList<Building>();
        coins = 500;
    }

    //methods

    public void addBuilding(Building b) {
        buildings.add(b);
    }

    public void addCharacter(Character c) {
        characters.add(c);
    }

}
