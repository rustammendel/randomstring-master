package tester;

//IMPORT LIST

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Window extends JFrame {

    private static CardLayout cl;
    private static JPanel windows;

    //FIELDS

    private GameWindow gameWindow;
    private JPanel mainWindow;
    private JPanel settingsWindow;
    private JPanel aboutWindow;
    private JPanel newGameWindow;

    private JLabel t1;
    private JButton b1;
    private JButton b2;
    private JButton b3;
    private JButton b4;
    private JButton b5;

    private JLabel l21;
    private JButton b21;
    private JLabel t5;
    private JButton b;

    //getters and setters


    public static JPanel getWindow() {
        return windows;
    }

    public static CardLayout getCL(){
        return cl;
    }

    //CONSTRUCTORS
    public Window() {
        setTitle("Window");
        setSize(1670, 1040); //TODO: automatically set window size according to tile size and count

        //panel to contain list of panels and make it card layout
        windows = new JPanel();
        windows.setLayout(new CardLayout());
        cl = (CardLayout) (windows.getLayout());

        mainWindow = new JPanel();
        mainWindow.setLayout(new BoxLayout(mainWindow, BoxLayout.Y_AXIS));
        t1 = new JLabel("MAIN MENU");
        b1 = new JButton("New Game");
        b1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                cl.show(windows, "newGameWindow");
            }
        });
        b2 = new JButton("Settings");
        b3 = new JButton("About");
        b3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                cl.show(windows, "aboutWindow");
            }
        });
        b4 = new JButton("Quit");
        b4.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                closeApp();
            }
        });
        b5 = new JButton("BACK TO GAME");
        b5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cl.show(windows, "gameWindow");
            }
        });
        mainWindow.add(b5);
        mainWindow.add(t1);
        mainWindow.add(b1);
        mainWindow.add(b2);
        mainWindow.add(b3);
        mainWindow.add(b4);

        //-----------------GAME WINDOW----------------------------------
        gameWindow = new GameWindow();


        //-----------------NEW GAME WINDOW------------------------------
        newGameWindow = new JPanel();
        l21 = new JLabel("NewGame");
        b21 = new JButton("BACK TO MAIN MENU");
        b21.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cl.show(windows, "mainWindow");
            }
        });

        newGameWindow.add(l21);
        newGameWindow.add(b21);

        //----------------SETTINGS WINDOW-------------------------------
        settingsWindow = new JPanel();

        //----------------ABOUT WINDOW-----------------------------------

        aboutWindow = new JPanel();
        t5 = new JLabel("ABOUT");
        b = new JButton("BACK TO MAIN MENU");
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cl.show(windows, "mainWindow");
            }
        });
        aboutWindow.add(t5);
        aboutWindow.add(b);

        //---------------------------------------------------------------

        windows.add(mainWindow, "mainWindow");
        windows.add(gameWindow, "gameWindow");
        windows.add(newGameWindow, "newGameWindow");
        windows.add(settingsWindow);
        windows.add(aboutWindow, "aboutWindow");

        this.add(windows);
        setVisible(true);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }

    public static void main(String[] arguments) {
        Window w = new Window();
        w.gameWindow.mapPanel.c.move(new Point(6, 1));
    }

    public CardLayout getCl() {
        return cl;
    }

    void closeApp() {
        this.dispose();
    }

}