package tester;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class Character extends Item implements ActionListener {
    public static int count = 0;
    public final int total_frames = 8;
    public String char_type;
    public int damage_level;
    public int life;
    //ANIMATION
    public int checkid;
    public ImageIcon images[][];
    public int current_frame = 0;
    public int button_dim = 40;
    public Point p;
    public Point pixel;
    public int direction = 0;
    public int block_size = 40;
    //public Point vector;
    public Timer t;
    public int i;
    public ArrayList<Point> path;
    public Point vector;
    public Point apath;
    public static String imagePath ="src\\resources\\character\\";


    //MOVING

    public Character() {
        count = count + 1;
        checkid = count;

        this.id = count;
        p = new Point(8, 8);
        pixel = new Point(0, 0);
        pixel.x = p.x * block_size;
        pixel.y = p.y * block_size;

        button = new JButton();
        button.setOpaque(false);
        button.setContentAreaFilled(false);
        button.setBorderPainted(false);
        button.setLayout(null);
        button.setBounds(pixel.x, pixel.y, button_dim, button_dim);
        button.addActionListener(this);


        /*
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });
        */

        this.images = new ImageIcon[total_frames][total_frames];
        for (int i = 0; i < total_frames; i++) {
            for (int j = 0; j < total_frames; ++j) {
                images[i][j] = new ImageIcon(this.imagePath + (i * total_frames + j) + ".png");
            }
        }

        button.setIcon(images[direction][current_frame]);

        try {
            button.setIcon(images[0][0]);
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    public void actionPerformed(ActionEvent e) {
        MapPanel.mode = 0;
        MapPanel.selected_char = this;
        ;
    }


    //METHODS
    public void move(Point ep) {

        Point tempp = new Point(ep.x, ep.y);


        System.out.println(checkid + "   Fuction called with parameters " + ep.x + " " + ep.y);
        GameEngine ge = new GameEngine();
        path = ge.getPath(this.p, ep);
        apath = new Point(path.get(0).x, path.get(0).y);
        System.out.println(checkid + "    NEXT ONE MOVE " + apath.x + " " + apath.y);
        vector = new Point(0, 0);

        vector.x = apath.x - p.x;
        vector.y = apath.y - p.y;

        //
        if (vector.x == 0 && vector.y == -1) {
            direction = 3;
        } else if (vector.x == 1 && vector.y == -1) {
            direction = 7;
        } else if (vector.x == 1 && vector.y == 0) {
            direction = 2;
        } else if (vector.x == 1 && vector.y == 1) {
            direction = 6;
        } else if (vector.x == 0 && vector.y == 1) {
            direction = 0;
        } else if (vector.x == -1 && vector.y == 1) {
            direction = 4;
        } else if (vector.x == -1 && vector.y == 0) {
            direction = 1;
        } else {
            direction = 5;
        }

        System.out.println("Timer started");

        t = new Timer(40, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if ((pixel.x != (apath.x * block_size) || (pixel.y != (apath.y) * block_size))) {
                    pixel.x = pixel.x + vector.x;
                    pixel.y = pixel.y + vector.y;
                    button.setBounds(pixel.x, pixel.y, block_size, block_size);
                    current_frame++;
                    button.setIcon(images[direction][current_frame % 8]);

                } else {
                    p.x = apath.x;
                    p.y = apath.y;
                    t.stop();
                    if (!(p.x == ep.x && p.y == ep.y)) {
                        move(tempp);
                    }
                }
            }

        });

        t.start();

    }
}