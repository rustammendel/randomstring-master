package tester;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;


public class MapPanel extends JPanel {

    //FIELDS

    public static int mode = 0;
    public static Character selected_char;
    public static int selected_build_type;
    //public int rows=38;
    //public int colums = 24;
    public int block_size = 40;
    public Character c;
    public Character c1;
    public Player player1;
    ImageIcon imgicn;
    Image bimg;
    JLabel backgnd;
    Point mousePoint;
    int button_dim = 40;
    JButton button;


    public MapPanel() {
        mousePoint = new Point(0, 0);

        bimg = null;
        try {
            bimg = ImageIO.read(new File("src\\resources\\map\\grid_new.png"));
        } catch (IOException e) {
            System.out.println("error");
        }
        imgicn = new ImageIcon(bimg);
        backgnd = new JLabel();
        try {
            backgnd.setIcon(imgicn);
            backgnd.setBounds(0, 0, 1520, 960);
        } catch (Exception ex) {
            System.out.println(ex);
        }

        this.setLayout(null);// set layout for map


        //GENERATING TEST
        GameEngine ge = new GameEngine();
        c = new Character();
        this.add(c.button);
        c1 = new Character();
        this.add(c1.button);

        //this.addMouseListener(this);
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                mousePoint.x = e.getX() / 40;
                mousePoint.y = e.getY() / 40;
                System.out.println(mousePoint.x + "                     " + mousePoint.y);


                if (mode == 0) {
                    System.out.println("mode0");
                    selected_char.move(mousePoint);
                } else if (mode == 1) {
                    System.out.println("mode1");
                    Character c = new Character();
                    add(c.button);

                    revalidate();
                    repaint();
                    setVisible(true);
                } else if (mode == 2) {
                    System.out.println("mode2");

                    if (selected_build_type == 4) {
                        if (
                                (GameEngine.runtime_obstacle_list[mousePoint.y * GameEngine.n_blocks_in_row + mousePoint.x] == 0) &&
                                        (GameEngine.obstacle_list[mousePoint.y * GameEngine.n_blocks_in_row + mousePoint.x] == 0) &&
                                        (GameEngine.runtime_obstacle_list[(mousePoint.y + 1) * GameEngine.n_blocks_in_row + mousePoint.x] == 0) &&
                                        (GameEngine.obstacle_list[(mousePoint.y + 1) * GameEngine.n_blocks_in_row + mousePoint.x] == 0) &&
                                        (GameEngine.runtime_obstacle_list[(mousePoint.y + 2) * GameEngine.n_blocks_in_row + mousePoint.x] == 0) &&
                                        (GameEngine.obstacle_list[(mousePoint.y + 2) * GameEngine.n_blocks_in_row + mousePoint.x] == 0)
                        ) {
                            Building b = new Building(MapPanel.selected_build_type, mousePoint);
                            add(b.button);
                            //player1.addBuilding(b);


                        }
                    } else if (
                            (GameEngine.runtime_obstacle_list[mousePoint.y * GameEngine.n_blocks_in_row + mousePoint.x] == 0) &&
                                    (GameEngine.obstacle_list[mousePoint.y * GameEngine.n_blocks_in_row + mousePoint.x] == 0) &&
                                    (GameEngine.runtime_obstacle_list[(mousePoint.y + 1) * GameEngine.n_blocks_in_row + mousePoint.x] == 0) &&
                                    (GameEngine.obstacle_list[(mousePoint.y + 1) * GameEngine.n_blocks_in_row + mousePoint.x] == 0) &&
                                    (GameEngine.runtime_obstacle_list[(mousePoint.y + 1) * GameEngine.n_blocks_in_row + (mousePoint.x + 1)] == 0) &&
                                    (GameEngine.obstacle_list[(mousePoint.y + 1) * GameEngine.n_blocks_in_row + (mousePoint.x + 1)] == 0) &&
                                    (GameEngine.runtime_obstacle_list[(mousePoint.y) * GameEngine.n_blocks_in_row + (mousePoint.x + 1)] == 0) &&
                                    (GameEngine.obstacle_list[(mousePoint.y) * GameEngine.n_blocks_in_row + (mousePoint.x + 1)] == 0)
                    ) {
                        System.out.println("this is okey ");
                        Building b = new Building(MapPanel.selected_build_type, mousePoint);
                        add(b.button);
                        // player1.addBuilding(b);
                    }

                    revalidate();
                    repaint();
                    setVisible(true);
                }

            }
        });


        setVisible(true);
        repaint();


    }

    @Override
    protected void paintComponent(Graphics g) {

        super.paintComponent(g);
        g.drawImage(bimg, 0, 0, null);
    }

}

