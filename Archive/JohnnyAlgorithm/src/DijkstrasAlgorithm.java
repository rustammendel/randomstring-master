
import java.util.ArrayList; // import the ArrayList class
import java.awt.*;

import static java.lang.StrictMath.sqrt;

class DijkstrasAlgorithm {

    private static final int NO_PARENT = -1;
    private static int dim=20;

    public ArrayList<Point> points = new ArrayList<Point>(); // Create an ArrayList object



    // Function that implements Dijkstra's single source shortest path algorithm for a graph represented using adjacency matrix representation
    private static ArrayList<Point> dijkstra(double[][] adjacencyMatrix, int startVertex,int endVertex,ArrayList<Point> points) {
        int nVertices = adjacencyMatrix[0].length;

        // shortestDistances[i] will hold the shortest distance from src to i
        double[] shortestDistances = new double[nVertices];

        // added[i] will true if vertex i is included in shortest path tree or shortest distance from src to i is finalized
        boolean[] added = new boolean[nVertices];

        // Initialize all distances as INFINITE and added[] as false
        for (int vertexIndex = 0; vertexIndex < nVertices;
             vertexIndex++) {
            shortestDistances[vertexIndex] = Double.MAX_VALUE;
            added[vertexIndex] = false;
        }

        // Distance of source vertex from itself is always 0
        shortestDistances[startVertex] = 0;

        // Parent array to store shortest path tree
        int[] parents = new int[nVertices];

        // The starting vertex does not have a parent
        parents[startVertex] = NO_PARENT;

        // Find shortest path for all vertices
        for (int i = 1; i < nVertices; i++) {

            // Pick the minimum distance vertex from the set of vertices not yet processed. nearestVertex is always equal to startNode in first iteration.
            int nearestVertex = -1;
            double shortestDistance = Double.MAX_VALUE;
            for (int vertexIndex = 0;
                 vertexIndex < nVertices;
                 vertexIndex++) {
                if (!added[vertexIndex] &&
                        shortestDistances[vertexIndex] <
                                shortestDistance) {
                    nearestVertex = vertexIndex;
                    shortestDistance = shortestDistances[vertexIndex];
                }
            }

            // Mark the picked vertex as processed
            added[nearestVertex] = true;

            // Update dist value of the adjacent vertices of the picked vertex.
            for (int vertexIndex = 0;
                 vertexIndex < nVertices;
                 vertexIndex++) {
                double edgeDistance = adjacencyMatrix[nearestVertex][vertexIndex];

                if (edgeDistance > 0
                        && ((shortestDistance + edgeDistance) <
                        shortestDistances[vertexIndex])) {
                    parents[vertexIndex] = nearestVertex;
                    shortestDistances[vertexIndex] = shortestDistance +
                            edgeDistance;
                }
            }
        }

        points = printTheSolution(startVertex,endVertex,parents,points);
        return points;
    }

    // A utility function to print the constructed distances array and shortest paths

    private static void printSolution(int startVertex, double[] distances, int[] parents,ArrayList<Point> points) {
        int nVertices = distances.length;

        for (int vertexIndex = 0;
             vertexIndex < nVertices;
             vertexIndex++) {
            if (vertexIndex != startVertex) {
                System.out.print("\n" + startVertex + " -> ");
                System.out.print(vertexIndex + " \t\t ");
                System.out.print(distances[vertexIndex] + "\t\t");
                printPath(vertexIndex, parents,points);
            }
        }
    }

    private static ArrayList<Point> printTheSolution(int startVertex,int endVertex, int[] parents,ArrayList<Point> points) {
        //System.out.print("\n" + startVertex + " -> ");
        //System.out.print(endVertex + " \t\t ");
        printPath(endVertex, parents,points);
        return points;
    }

    // Function to print shortest path from source to currentVertex using parents array

    private static ArrayList<Point> printPath(int currentVertex, int[] parents,ArrayList<Point> points) {
        // Base case : Source node has been processed
        if (currentVertex == NO_PARENT) {
            return points;
        }
        printPath(parents[currentVertex], parents,points);
        //System.out.print(currentVertex + " ");
        points.add(new Point(currentVertex/dim,currentVertex%dim));
        return points;
    }





    // Driver Code
    public static void main(String[] args) {

        ArrayList<Point> points = new ArrayList<Point>(); // Create an ArrayList object

        int pix = dim * dim;
        double[][] adjacencyMatrix = new double[pix][pix];

        for (int i = 0; i < pix; ++i) {
            for (int j = 0; j < pix; ++j) {
                if ((i == 0) || (i == dim - 1) || (i == dim * (dim - 1)) || (i == dim * dim - 1)) { //case corner
                    if (i == 0) {
                        if (j == i + 1 || j == i + dim) {
                            adjacencyMatrix[i][j] = 1;
                        } else if (j == i + dim + 1) {
                            adjacencyMatrix[i][j] = sqrt(2);
                        } else {
                            adjacencyMatrix[i][j] = 0;
                        }
                    } else if (i == dim - 1) {
                        if (j == i - 1 || j == i + dim) {
                            adjacencyMatrix[i][j] = 1;
                        } else if (j == i + dim - 1) {
                            adjacencyMatrix[i][j] = sqrt(2);
                        } else {
                            adjacencyMatrix[i][j] = 0;
                        }
                    } else if (i == dim * (dim - 1)) {
                        if (j == i + 1 || j == i - dim) {
                            adjacencyMatrix[i][j] = 1;
                        } else if (j == i - dim + 1) {
                            adjacencyMatrix[i][j] = sqrt(2);
                        } else {
                            adjacencyMatrix[i][j] = 0;
                        }
                    } else {
                        if (j == i - 1 || j == i - dim) {
                            adjacencyMatrix[i][j] = 1;
                        } else if (j == i - dim - 1) {
                            adjacencyMatrix[i][j] = sqrt(2);
                        } else {
                            adjacencyMatrix[i][j] = 0;
                        }
                    }
                } else if ((0 < i && i < dim) || (dim * (dim - 1) < i && i < dim * dim - 1) || (i % dim == 0) || (i % dim == dim - 1)) { //case sides
                    if (0 < i && i < dim) {
                        if (j == i - 1 || j == i + 1 || j == i + dim) {
                            adjacencyMatrix[i][j] = 1;
                        } else if (j == i + dim - 1 || j == i + dim + 1) {
                            adjacencyMatrix[i][j] = sqrt(2);
                        } else {
                            adjacencyMatrix[i][j] = 0;
                        }
                    } else if (dim * (dim - 1) < i && i < dim * dim - 1) {
                        if (j == i - 1 || j == i + 1 || j == i - dim) {
                            adjacencyMatrix[i][j] = 1;
                        } else if (j == i - dim - 1 || j == i - dim + 1) {
                            adjacencyMatrix[i][j] = sqrt(2);
                        } else {
                            adjacencyMatrix[i][j] = 0;
                        }
                    } else if (i % dim == 0) {
                        if (j == i + 1 || j == i - dim || j == i + dim) {
                            adjacencyMatrix[i][j] = 1;
                        } else if (j == i - dim + 1 || j == i + dim + 1) {
                            adjacencyMatrix[i][j] = sqrt(2);
                        } else {
                            adjacencyMatrix[i][j] = 0;
                        }
                    } else {
                        if (j == i - 1 || j == i - dim || j == i + dim) {
                            adjacencyMatrix[i][j] = 1;
                        } else if (j == i - dim - 1 || j == i + dim - 1) {
                            adjacencyMatrix[i][j] = sqrt(2);
                        } else {
                            adjacencyMatrix[i][j] = 0;
                        }
                    }
                } else { //case center
                    if (j == i + 1 || j == i - 1 || j == i - dim || j == i + dim) {
                        adjacencyMatrix[i][j] = 1;
                    } else if (j == i - dim - 1 || j == i - dim + 1 || j == i + dim - 1 || j == i + dim + 1) {
                        adjacencyMatrix[i][j] = sqrt(2);
                    } else {
                        adjacencyMatrix[i][j] = 0;
                    }
                }
            }
        }

        Point sp = new Point(0,0); //Starting Point

        Point ep = new Point(8,4); //Ending Point
        System.out.print("(" + sp.x +" , " + sp.y + ") ->" );
        System.out.println("(" + ep.x +" , " + ep.y + ") " );
        int startVertex = sp.x * dim + sp.y;
        int endVertex = ep.x * dim + ep.y;
        dijkstra(adjacencyMatrix, startVertex,endVertex,points);
        for(int i=0;i<points.size();++i){
            System.out.print("("+points.get(i).getX() + " , " + points.get(i).getY()+")" + " -> ");
        }
        System.out.println("");
    }
}
