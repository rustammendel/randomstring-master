package tester;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Character extends Item{
    protected String char_type;
    protected int damage_level;
    protected int life;

    //ANIMATION
    protected ImageIcon images[][];
    protected final int total_frames = 8;
    protected int current_frame =0;
    public int button_dim = 40;
    public Point p;
    public int direction = 0;

    public Character(){
        p = new Point(0,0);

        button = new JButton();
        button.setOpaque(false);
        button.setContentAreaFilled(false);
        button.setBorderPainted(false);
        button.setLayout(null);
        button.setBounds(0, 0, button_dim,button_dim);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("button clicked");
            }
        });

        this.images = new ImageIcon[total_frames][total_frames];
        for (int i = 0; i < total_frames; i++) {
            for (int j = 0; j < total_frames; ++j) {
                images[i][j] = new ImageIcon("imgs/" + (i*total_frames+j) + ".png");
            }
        }
        /*
        button.setIcon(images[direction][current_frame]);

        try {
            //button.setIcon(images[0][0]);
        } catch (Exception ex) {
            System.out.println(ex);
        }
        */



    }


}


