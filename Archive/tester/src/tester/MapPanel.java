package tester;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.String;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

public class MapPanel extends JPanel{

    //FIELDS

    //public int rows=38;
    //public int colums = 24;
    public int block_size = 40;

    ImageIcon imgicn;
    Image bimg;
    JLabel backgnd;
    public Point mousePoint;
    Character selected_c;
    int button_dim =40;
    JButton button;
    public Character testchar;


    public MapPanel(){
        mousePoint = new Point(3,3);
            button = new JButton();
            button.setBounds(1, 1, 20,20);

            this.add(button);

        bimg = null;
        try {
            bimg = ImageIO.read(new File("./revised.png"));
        } catch (IOException e) {
            System.out.println("error");
        }

        imgicn = new ImageIcon(bimg);
        backgnd = new JLabel();
        try {
            backgnd.setIcon(imgicn);
            backgnd.setBounds(0,0,1520,960);
        } catch (Exception ex) {
            System.out.println(ex);
        }

        this.setLayout(null);// set layout for map


        testchar = new Character();
        testchar.button.setBounds(0,0,40,40);
        this.add(testchar.button);


        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
               // mousePoint = new Point(3,3);
                mousePoint.x = e.getX()/40;
                mousePoint.y = e.getY()/40;
                System.out.println("Mouse Clicked: ("+e.getX()+", "+e.getY() +")");
                System.out.println(mousePoint.x +"                     " + mousePoint.y);
                System.out.println("current position " + testchar.p.x + " , " + testchar.p.y);
                GameEngine ge = new GameEngine();
                ge.moveCharacter(testchar,mousePoint);
            }
        });


        setVisible(true);
        repaint();


    }

    @Override
    protected void paintComponent(Graphics g) {

        super.paintComponent(g);
        g.drawImage(bimg, 0, 0, null);
    }

}

