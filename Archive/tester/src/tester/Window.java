package tester;

//IMPORT LIST
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.CardLayout;


public class Window extends JFrame{

    //FIELDS
    //WHY STATIC??????
    public GameWindow a;
    public static CardLayout cl;
    public  static JPanel windows;
    public GameWindow gameWindow;
    public JPanel mainWindow;
    public JPanel settingsWindow;
    public JPanel aboutWindow;
    public JPanel newGameWindow;

    public JLabel t1;
    public JButton b1;
    public JButton b2;
    public JButton b3;
    public JButton b4;
    public JButton b5;

    public JLabel l21;
    public JButton b21;
    public JLabel t5;
    public JButton b;




    //CONSTRUCTORS
    public Window(){
        setTitle("Window");
        setExtendedState(JFrame.MAXIMIZED_BOTH);

        //panel to contain list of panels and make it card layout
        windows = new JPanel();
        windows.setLayout(new CardLayout());
        cl = (CardLayout)(windows.getLayout());

            mainWindow = new JPanel();
            mainWindow.setLayout(new BoxLayout(mainWindow,BoxLayout.Y_AXIS));
                t1=new JLabel("MAIN MENU");
                b1 = new JButton("New Game");
                b1.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        cl.show(windows,"newGameWindow");
                    }
                 });
                b2 = new JButton("Settings");
                b3 = new JButton("About");
                b3.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        cl.show(windows,"aboutWindow");
                    }
                });
                b4 = new JButton("Quit");
                b4.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        closeApp();
                    }
                });
                b5 = new JButton("BACK TO GAME");
                b5.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        cl.show(windows,"gameWindow");
                    }
                });
            mainWindow.add(b5);
            mainWindow.add(t1);
            mainWindow.add(b1);
            mainWindow.add(b2);
            mainWindow.add(b3);
            mainWindow.add(b4);

            //-----------------GAME WINDOW----------------------------------
            gameWindow = new GameWindow();


            //-----------------NEW GAME WINDOW------------------------------
            newGameWindow = new JPanel();
                l21 = new JLabel("NewGame");
                b21 = new JButton("BACK TO MAIN MENU");
                b21.addActionListener(new ActionListener() {
                @Override
                    public void actionPerformed(ActionEvent e) {
                        cl.show(windows,"mainWindow");
                    }
                });

            newGameWindow.add(l21);
            newGameWindow.add(b21);

            //----------------SETTINGS WINDOW-------------------------------
            settingsWindow = new JPanel();

            //----------------ABOUT WINDOW-----------------------------------

            aboutWindow = new JPanel();
                t5 = new JLabel("ABOUT");
                b = new JButton("BACK TO MAIN MENU");
                b.addActionListener(new ActionListener() {
                @Override
                    public void actionPerformed(ActionEvent e) {
                        cl.show(windows,"mainWindow");
                     }
                });
            aboutWindow.add(t5);
            aboutWindow.add(b);

            //---------------------------------------------------------------

        windows.add(mainWindow,"mainWindow");
        windows.add(gameWindow,"gameWindow");
        windows.add(newGameWindow,"newGameWindow");
        windows.add(settingsWindow);
        windows.add(aboutWindow,"aboutWindow");

    this.add(windows);
    setVisible(true);



    }


    public CardLayout getCl(){
        return cl;
    }


    void closeApp(){
        this.dispose();
    }

    public static void main(String[] arguments) {
        Window w = new Window();

        GameEngine ge = new GameEngine();
        //Character testchar = new Character();
        //w.gameWindow.mapPanel.add(testchar.button);
        //testchar.button.setBounds(0,0,40,40);
        Point np = new Point(w.gameWindow.mapPanel.mousePoint.x,w.gameWindow.mapPanel.mousePoint.y);
        System.out.println("this is the point "+np);
       // ge.moveCharacter(w.gameWindow.mapPanel.testchar, np);


    }

}