import javax.swing.*;
import java.awt.*;

public class GameWindow extends JFrame {

    public GameWindow(){
        super("Title");
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());

        JPanel top_p = new JPanel();
        top_p.setLayout(new GridLayout(1,3,10,0));
        top_p.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        JButton menu_b = new JButton("MENU");
        JLabel health_l = new JLabel("Health: ");
        JLabel coins_l = new JLabel("Coins: ");
        JLabel soldiers_l = new JLabel("Soldiers: ");
        JLabel buildings_l = new JLabel("Buildings: ");

        top_p.add(health_l);
        top_p.add(coins_l);
        top_p.add(soldiers_l);
        top_p.add(buildings_l);
        top_p.add(menu_b);



        JPanel side_p = new JPanel();
        side_p.setBorder(BorderFactory.createLineBorder(Color.BLACK));

        JPanel add_p= new JPanel();
        add_p.setLayout(new GridLayout(4,2,10,10));
        add_p.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        JLabel item1 = new JLabel("Item1");
        JLabel item2 = new JLabel("Item2");
        JLabel item3 = new JLabel("Item3");
        JLabel item4 = new JLabel("Item4");
        JLabel item5 = new JLabel("Item5");
        JLabel item6 = new JLabel("Item6");
        JLabel item7 = new JLabel("Item7");
        JLabel item8 = new JLabel("Item8");


        add_p.add(item1);
        add_p.add(item2);
        add_p.add(item3);
        add_p.add(item4);
        add_p.add(item5);
        add_p.add(item6);
        add_p.add(item7);
        add_p.add(item8);


        side_p.add(add_p);


        JPanel map_p = new JPanel();

        map_p.setBorder(BorderFactory.createLineBorder(Color.BLACK));

        //map












        this.add(top_p,BorderLayout.NORTH);
        this.add(side_p,BorderLayout.WEST);
        this.add(map_p,BorderLayout.CENTER);

        //coins.setText("Coins: 500000");
        setVisible(true);
    }

    public static void main(String[] arguments) {
        GameWindow gw = new GameWindow();

    }



}

