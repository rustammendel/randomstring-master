import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Window extends JFrame{
    public Window(){
        setTitle("Window");
        //CardLayout cl = new CardLayout();
        //setLayout(new CardLayout());
        setSize(400,400);
        //setExtendedState(JFrame.MAXIMIZED_BOTH);

        JPanel windows = new JPanel();
        windows.setLayout(new CardLayout());

            JPanel MainWindow = new JPanel();
            MainWindow.setLayout(new BoxLayout(MainWindow,BoxLayout.Y_AXIS));
                JLabel t1=new JLabel("MAIN MENU");
                JButton b1 = new JButton("New Game");
                b1.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                            //Execute when button is pressed
                            System.out.println("You clicked the button");
                            CardLayout cl = (CardLayout)(windows.getLayout());
                            cl.show(windows,"NewGameWindow");
                    }
                });
                JButton b2 = new JButton("Settings");
                JButton b3 = new JButton("About");
                b3.addActionListener(new ActionListener() {

                    public void actionPerformed(ActionEvent e)
                    {
                        //Execute when button is pressed
                        System.out.println("You clicked the button");
                        CardLayout cl = (CardLayout)(windows.getLayout());
                        cl.show(windows,"AboutWindow");
                    }
                });
        JButton b4 = new JButton("Quit");
        b4.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e)
            {
                System.out.println("You clicked the button");
                closeApp();
                System.out.println("Frame Closed.");
            }
        });
            MainWindow.add(t1);
            MainWindow.add(b1);
            MainWindow.add(b2);
            MainWindow.add(b3);
            MainWindow.add(b4);

            //-----------------GAME WINDOW----------------------------------
            JPanel GameWindow = new JPanel();

            //-----------------NEW GAME WINDOW------------------------------
            JPanel NewGameWindow = new JPanel();
                JLabel l21 = new JLabel("NewGame");
                JButton b21 = new JButton("BACK TO MAIN MENU");
                b21.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        System.out.println("You clicked the button");
                        CardLayout cl = (CardLayout)(windows.getLayout());
                        cl.show(windows,"MainWindow");
                    }
                });
            NewGameWindow.add(l21);
            NewGameWindow.add(b21);

            //----------------SETTINGS WINDOW-------------------------------
            JPanel SettingsWindow = new JPanel();

            //----------------ABOUT WINDOW-----------------------------------

            JPanel AboutWindow = new JPanel();
                JLabel t5 = new JLabel("ABOUT");
                JButton b = new JButton("BACK TO MAIN MENU");
                b.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        System.out.println("You clicked the button");
                        CardLayout cl = (CardLayout)(windows.getLayout());
                        cl.show(windows,"MainWindow");
                    }
                });
            AboutWindow.add(t5);
            AboutWindow.add(b);

            //---------------------------------------------------------------

        windows.add(MainWindow,"MainWindow");
        windows.add(GameWindow,"GameWindow");
        windows.add(NewGameWindow,"NewGameWindow");
        windows.add(SettingsWindow);
        windows.add(AboutWindow,"AboutWindow");

        this.add(windows);

        //CardLayout cl = (CardLayout)(this.getLayout());
        //cl.show(cards, (String)evt.getItem());

        setVisible(true);
    }

    void closeApp(){
        this.dispose();
    }


}


